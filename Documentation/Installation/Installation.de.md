# Installation

Die Installation ist wie auch bei der bekannten tt_address Erweiternug sehr einfach:

1. Installiere den address_manager über den Erweiterungsmanager oder mittels composer:
   ```
   composer req codingms/address-manager
   ```
2. Lege einen Sys-Ordner an, in dem Du die Address-Datensätze anlegst. Bei Bedarf kannst Du außerdem
   Datensätze vom Typ "Gruppe", "Organisation" oder "Position" anlegen.
3. Legen eine Seite an, auf der Du die Adressdatensätze anzeigen lassen möchtest. Auf dieser Seite legst Du ein
   Plugin vom Typ "Address-Manager - List" an.
4. Setze im Konstanten-Editor unter "Address-Manager" den Wert für "Container for address records"
   (themes.configuration.container.address), trage die ID des Sys-Ordners ein, auf dem die Adressen angelegt sind.

> #### Wichtiger Hinweis zu Gruppen, Organisationen und Positionen {.alert .alert-danger}
>
> Wenn Gruppen, Organisationen oder Positionen angelegt sind, muss auch jede Adresse eine Gruppe, Organisation oder Position zugewiesen bekommen, da sonst keine Inhalte angezeigt werden oder es zu ungewollten Suchergebnissen kommen kann.
>
> Gruppe, Position und Organisation müssen im Plugin-Datensatz *Filter-Felder* als erstes ausgewählt werden, andernfalls kann es sein das diese Felder im Frontend deaktiviert angezeigt werden!
