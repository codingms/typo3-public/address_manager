# Open address list using address group parameter

Address groups in AddressManager have a few additional features. The Addressgroup-Teaser plugin allows the address group to be passed to the address list as a parameter so that addresses from that group can be displayed. The parameter looks as follows:

```text
?tx_addressmanager_addresslist%5BgroupSelect%5D=42
bzw
?tx_addressmanager_addresslist[groupSelect]=42
```
