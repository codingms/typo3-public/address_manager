# Datenschutz-Cookie via Link entfernen

Um den Datenschutz-Cookie des Address-Managers zu entfernen, kann ein Script wie folgt verwendet werden:

```html
<script>
	function delete_cookie(name) {
		document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		alert('Zustimmung gelöscht');
	}
</script>
<h4>Google-Maps</h4>
<p>
	Klicken Sie auf den Button, um die automatische Darstellung von Google-Maps zu deaktivieren.
	Das Cookie, welches Ihre Zustimmung zum automatischen Laden speichert, wird dann gelöscht.
</p>
<a href="#" onclick="delete_cookie('googleMapsAllowed');" class="btn btn-primary">Google-Maps deaktivieren</a>
```
