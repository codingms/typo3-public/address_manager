# Geo-Koordinaten mit den Scheduler ermitteln

Die Address-Manager-Erweiterung bietet einen CommandController im Scheduler, mit dem Du einfach die Geo-Koordinaten für Deine Adressen ermitteln kannst. Dafür wird die Google-Maps API Schnittstelle verwendet.

Dieser Scheduler bietet folgende Parameter:

*   **apiKey:** Ein Google-Maps API-Key, welcher über eine serverseitige Restriction verfügt und die APIs GeoCoding und Places freigeschaltet hat.
*   **useFields:** Eine komma separierte Liste mit Datenbank-Feldern aus der Adress-Tabelle, mit welchen die Adresse ermittelt werden soll.
*   **limit:** Maximale Anzahl an Adressen die in einer Ausführung verarbeitet werden sollen. 0 bedeutet kein Limit.
