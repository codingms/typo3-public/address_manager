# JavaScript

## JavaScript-Events

Es werden folgende JavaScript-Events geworfen:

*   `address_manager.after_initialize` wird geworfen, wenn der AddressManager initialisiert wurde.
*   `address_manager.result` wird geworfen, wenn der AddressManager ein Ergebnis gefunden hat.
*   `address_manager.no_result` wird geworfen, wenn der AddressManager kein Ergebnis gefunden hat.

Die Events werden wie folgt verwendet:

```javascript
jQuery(window).on('address_manager.result', function() {
    alert('test')
});
```

## Tooltip-Inhalt anpassen

Um den Tooltip-Inhalt der Maps-Marker anzupassen, kann einfach das entsprechende Data-Attribut angepasst werden:

```xml
<div class="address-list-item card"
	 data-map-latitude="{address.mapLatitude}"
	 data-map-longitude="{address.mapLongitude}"
	 data-map-tooltip="{address.mapTooltip} <b>Add own custom tooltip content!</b>"
	 data-map-link="{address.mapLink}"
	 data-map-marker="{am:variable.get(name: 'settings.list.map.marker.style.{address.mapMarker}')}">
...
</div>
```
