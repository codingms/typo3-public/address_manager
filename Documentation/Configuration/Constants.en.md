# TypoScript constant settings


## General

| **Constant**      | themes.configuration.siteName                                                |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Site name                                                                    |
| **Description**   |                                                                              |
| **Type**          | string                                                                       |
| **Default value** |                                                                              |



## Address Pages

| **Constant**      | themes.configuration.pages.address.list                                      |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Page for address list                                                        |
| **Description**   |                                                                              |
| **Type**          | int+                                                                         |
| **Default value** | 0                                                                            |

| **Constant**      | themes.configuration.pages.address.detail                                    |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Page for address details                                                     |
| **Description**   |                                                                              |
| **Type**          | int+                                                                         |
| **Default value** | 0                                                                            |



## Address Container

| **Constant**      | themes.configuration.container.address                                       |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Container for address records                                                |
| **Description**   |                                                                              |
| **Type**          | int+                                                                         |
| **Default value** | 0                                                                            |



## Address-Manager Templates

| **Constant**      | themes.configuration.extension.address_manager.view.templateRootPath         |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Template root path                                                           |
| **Description**   |                                                                              |
| **Type**          | string                                                                       |
| **Default value** |                                                                              |

| **Constant**      | themes.configuration.extension.address_manager.view.partialRootPath          |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Partial root path                                                            |
| **Description**   |                                                                              |
| **Type**          | string                                                                       |
| **Default value** |                                                                              |

| **Constant**      | themes.configuration.extension.address_manager.view.layoutRootPath           |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Layout root path                                                             |
| **Description**   |                                                                              |
| **Type**          | string                                                                       |
| **Default value** |                                                                              |



## Address list view

| **Constant**      | themes.configuration.extension.address_manager.list.image.width              |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | List image width                                                             |
| **Description**   | This value is used as max value.                                             |
| **Type**          | string                                                                       |
| **Default value** | 320                                                                          |

| **Constant**      | themes.configuration.extension.address_manager.list.image.height             |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | List image height                                                            |
| **Description**   | This value is used as max value.                                             |
| **Type**          | string                                                                       |
| **Default value** | 240                                                                          |

| **Constant**      | themes.configuration.extension.address_manager.list.group.sortBy             |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | List group                                                                   |
| **Description**   | Field for sorting groups.                                                    |
| **Type**          | Selectbox with options: ,                                                    |
| **Default value** |                                                                              |

| **Constant**      | themes.configuration.extension.address_manager.list.group.sortOrder          |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | List group                                                                   |
| **Description**   | Sort order for groups.                                                       |
| **Type**          | Selectbox with options: ,                                                    |
| **Default value** |                                                                              |

| **Constant**      | themes.configuration.extension.address_manager.list.organisation.sortBy      |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | List organisation                                                            |
| **Description**   | Field for sorting organisations.                                             |
| **Type**          | Selectbox with options: ,                                                    |
| **Default value** |                                                                              |

| **Constant**      | themes.configuration.extension.address_manager.list.organisation.sortOrder   |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | List organisation                                                            |
| **Description**   | Sort order for organisations.                                                |
| **Type**          | Selectbox with options: ,                                                    |
| **Default value** |                                                                              |

| **Constant**      | themes.configuration.extension.address_manager.list.position.sortBy          |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | List position                                                                |
| **Description**   | Field for sorting positions.                                                 |
| **Type**          | Selectbox with options: ,                                                    |
| **Default value** |                                                                              |

| **Constant**      | themes.configuration.extension.address_manager.list.position.sortOrder       |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | List position                                                                |
| **Description**   | Sort order for positions.                                                    |
| **Type**          | Selectbox with options: ,                                                    |
| **Default value** |                                                                              |

| **Constant**      | themes.configuration.extension.address_manager.list.location.submitOnEnter   |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Location filter                                                              |
| **Description**   | Submit location filter on presssing enter key.                               |
| **Type**          | boolean                                                                      |
| **Default value** | 0                                                                            |

| **Constant**      | themes.configuration.extension.address_manager.list.searchWord.submitOnEnter |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Text search filter                                                           |
| **Description**   | Submit search on presssing enter key.                                        |
| **Type**          | boolean                                                                      |
| **Default value** | 0                                                                            |

| **Constant**      | themes.configuration.extension.address_manager.list.customString.field       |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | List custom string                                                           |
| **Description**   | Fields to search in, you can define multiple comma-separated fields.         |
| **Type**          | string                                                                       |
| **Default value** | first_name,last_name,company,description                                     |

| **Constant**      | themes.configuration.extension.address_manager.list.orderForAjaxResult       |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | List order for Ajax result.                                                  |
| **Description**   |                                                                              |
| **Type**          | Selectbox with options: ,                                                    |
| **Default value** |                                                                              |



## Address list view - maps

| **Constant**      | themes.configuration.extension.address_manager.list.map.clustering.active    |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Activate marker clustering                                                   |
| **Description**   |                                                                              |
| **Type**          | Selectbox with options: 0, 1                                                 |
| **Default value** | 0                                                                            |

| **Constant**      | themes.configuration.extension.address_manager.list.map.clustering.imagePath |
| :---------------- |:-----------------------------------------------------------------------------|
| **Label**         | Path for clustering images                                                   |
| **Description**   |                                                                              |
| **Type**          | string                                                                       |
| **Default value** | EXT:address_manager/Resources/Public/Images/MapClustering/m                  |

| **Constant**      | themes.configuration.extension.address_manager.list.map.default.latitude     |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Default Latitude                                                             |
| **Description**   | Is used as center when there are no marker on load.                          |
| **Type**          | string                                                                       |
| **Default value** | 51.96066490000001                                                            |

| **Constant**      | themes.configuration.extension.address_manager.list.map.default.longitude    |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Default Longitude                                                            |
| **Description**   | Is used as center when there are no marker on load.                          |
| **Type**          | string                                                                       |
| **Default value** | 7.626134699999966                                                            |

| **Constant**      | themes.configuration.extension.address_manager.list.map.countryRestriction   |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Country restriction for map                                                  |
| **Description**   |                                                                              |
| **Type**          | string                                                                       |
| **Default value** | de                                                                           |

| **Constant**      | themes.configuration.extension.address_manager.list.map.maxZoom              |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Maximum zoom - used to set viewport to marker bounds                         |
| **Description**   |                                                                              |
| **Type**          | int+                                                                         |
| **Default value** | 13                                                                           |

| **Constant**      | themes.configuration.extension.address_manager.list.map.style                |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Options for map styling                                                      |
| **Description**   |                                                                              |
| **Type**          | Selectbox with options: , ,                                                  |
| **Default value** |                                                                              |



## Address detail view

| **Constant**      | themes.configuration.extension.address_manager.detail.image.width            |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Detail image width                                                           |
| **Description**   | This value is used as max value.                                             |
| **Type**          | string                                                                       |
| **Default value** | 600                                                                          |

| **Constant**      | themes.configuration.extension.address_manager.detail.image.height           |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Detail image height                                                          |
| **Description**   | This value is used as max value.                                             |
| **Type**          | string                                                                       |
| **Default value** | 400                                                                          |

| **Constant**      | themes.configuration.extension.address_manager.detail.map.zoom               |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Detail map zoom                                                              |
| **Description**   |                                                                              |
| **Type**          | int+                                                                         |
| **Default value** | 17                                                                           |

| **Constant**      | themes.configuration.extension.address_manager.detail.map.style              |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Detail map style                                                             |
| **Description**   | Options for data-style.                                                      |
| **Type**          | Selectbox with options: , ,                                                  |
| **Default value** |                                                                              |



## Google Maps Consent

| **Constant**      | themes.configuration.extension.address_manager.googleMapsConsent.active      |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Show button and ask for consent before loading Google Maps                   |
| **Description**   |                                                                              |
| **Type**          | Selectbox with options: 0, 1                                                 |
| **Default value** | 0                                                                            |



## Javascript

| **Constant**      | themes.configuration.javascript.google.maps.apiKey                           |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Google Maps api key                                                          |
| **Description**   |                                                                              |
| **Type**          | string                                                                       |
| **Default value** |                                                                              |

| **Constant**      | themes.configuration.javascript.google.maps.libraries                        |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Google Maps libraries                                                        |
| **Description**   |                                                                              |
| **Type**          | string                                                                       |
| **Default value** | places                                                                       |



## Framework

| **Constant**      | themes.framework                                                             |
| :---------------- | :--------------------------------------------------------------------------- |
| **Label**         | Framework                                                                    |
| **Description**   |                                                                              |
| **Type**          | string                                                                       |
| **Default value** | Bootstrap4                                                                   |



