# JavaScript

## JavaScript Events

The following Javascript events are triggered:

*   `address_manager.after_initialize` triggered when the AddressManager is initialized.
*   `address_manager.result` triggered when the AddressManager finds a result
*   `address_manager.no_result` triggered when the AddressManager doesn't find a result

Events are used as follows:

```javascript
jQuery(window).on('address_manager.result', function() {
    alert('test')
});
```

## Adjust Tooltip content

In order to customize the tooltip content of the maps markers, you can simply adjust the corresponding data attribute:

```xml
<div class="address-list-item card"
	 data-map-latitude="{address.mapLatitude}"
	 data-map-longitude="{address.mapLongitude}"
	 data-map-tooltip="{address.mapTooltip} <b>Add own custom tooltip content!</b>"
	 data-map-link="{address.mapLink}"
	 data-map-marker="{am:variable.get(name: 'settings.list.map.marker.style.{address.mapMarker}')}">
...
</div>
```
