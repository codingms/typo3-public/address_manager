# TypoScript-Konstanten Einstellungen


## Allgemein

| **Konstante**    | themes.configuration.siteName                                                                                                      |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Webseiten-Name                                                                                                                     |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | string                                                                                                                             |
| **Standardwert** |                                                                                                                                    |



## Adress-Seiten

| **Konstante**    | themes.configuration.pages.address.list                                                                                            |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Seite für Adressen-Liste                                                                                                           |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | int+                                                                                                                               |
| **Standardwert** | 0                                                                                                                                  |

| **Konstante**    | themes.configuration.pages.address.detail                                                                                          |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Seite für Adress-Details                                                                                                           |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | int+                                                                                                                               |
| **Standardwert** | 0                                                                                                                                  |



## Adressen-Container

| **Konstante**    | themes.configuration.container.address                                                                                             |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Container für Adress-Datensätze                                                                                                    |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | int+                                                                                                                               |
| **Standardwert** | 0                                                                                                                                  |



## Adressen-Manager Templates

| **Konstante**    | themes.configuration.extension.address_manager.view.templateRootPath                                                               |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Template root path                                                                                                                 |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | string                                                                                                                             |
| **Standardwert** |                                                                                                                                    |

| **Konstante**    | themes.configuration.extension.address_manager.view.partialRootPath                                                                |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Partial root path                                                                                                                  |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | string                                                                                                                             |
| **Standardwert** |                                                                                                                                    |

| **Konstante**    | themes.configuration.extension.address_manager.view.layoutRootPath                                                                 |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Layout root path                                                                                                                   |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | string                                                                                                                             |
| **Standardwert** |                                                                                                                                    |



## Adress-Listenansicht.

| **Konstante**    | themes.configuration.extension.address_manager.list.image.width                                                                    |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Liste Bildbreite                                                                                                                   |
| **Beschreibung** | Dieser Wert wird als maximum Wert benutzt.                                                                                         |
| **Typ**          | string                                                                                                                             |
| **Standardwert** | 320                                                                                                                                |

| **Konstante**    | themes.configuration.extension.address_manager.list.image.height                                                                   |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Liste Bildhöhe                                                                                                                     |
| **Beschreibung** | Dieser Wert wird als maximum Wert benutzt.                                                                                         |
| **Typ**          | string                                                                                                                             |
| **Standardwert** | 240                                                                                                                                |

| **Konstante**    | themes.configuration.extension.address_manager.list.group.sortBy                                                                   |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Listen-Gruppe                                                                                                                      |
| **Beschreibung** | Feld zum Sortieren von Gruppen                                                                                                     |
| **Typ**          | Auswahlbox mit Optionen: ,                                                                                                         |
| **Standardwert** |                                                                                                                                    |

| **Konstante**    | themes.configuration.extension.address_manager.list.group.sortOrder                                                                |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Listen-Gruppe                                                                                                                      |
| **Beschreibung** | Sortierung für Gruppen                                                                                                             |
| **Typ**          | Auswahlbox mit Optionen: ,                                                                                                         |
| **Standardwert** |                                                                                                                                    |

| **Konstante**    | themes.configuration.extension.address_manager.list.organisation.sortBy                                                            |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Listen-Organisation                                                                                                                |
| **Beschreibung** | Feld zum Sortieren von Organisationen                                                                                              |
| **Typ**          | Auswahlbox mit Optionen: ,                                                                                                         |
| **Standardwert** |                                                                                                                                    |

| **Konstante**    | themes.configuration.extension.address_manager.list.organisation.sortOrder                                                         |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Listen-Organisation                                                                                                                |
| **Beschreibung** | Sortierung für Organisationen                                                                                                      |
| **Typ**          | Auswahlbox mit Optionen: ,                                                                                                         |
| **Standardwert** |                                                                                                                                    |

| **Konstante**    | themes.configuration.extension.address_manager.list.position.sortBy                                                                |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Listen-Position                                                                                                                    |
| **Beschreibung** | Feld zum Sortieren von Positionen                                                                                                  |
| **Typ**          | Auswahlbox mit Optionen: ,                                                                                                         |
| **Standardwert** |                                                                                                                                    |

| **Konstante**    | themes.configuration.extension.address_manager.list.position.sortOrder                                                             |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Listen-Position                                                                                                                    |
| **Beschreibung** | Sortierung für Positionen.                                                                                                         |
| **Typ**          | Auswahlbox mit Optionen: ,                                                                                                         |
| **Standardwert** |                                                                                                                                    |

| **Konstante**    | themes.configuration.extension.address_manager.list.location.submitOnEnter                                                         |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Standortfilter                                                                                                                     |
| **Beschreibung** | Senden Sie den Standortfilter beim Drücken der Eingabetaste.                                                                       |
| **Typ**          | boolean                                                                                                                            |
| **Standardwert** | 0                                                                                                                                  |

| **Konstante**    | themes.configuration.extension.address_manager.list.searchWord.submitOnEnter                                                       |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Suchwortfilter                                                                                                                     |
| **Beschreibung** | Suche beim Drücken der Eingabetaste senden.                                                                                        |
| **Typ**          | boolean                                                                                                                            |
| **Standardwert** | 0                                                                                                                                  |

| **Konstante**    | themes.configuration.extension.address_manager.list.customString.field                                                             |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Listen-Custom-String                                                                                                               |
| **Beschreibung** | Benutzerdefinierte Zeichenfolge auflistenIn den zu suchenden Feldern können Sie mehrere durch Kommas getrennte Felder definieren.  |
| **Typ**          | string                                                                                                                             |
| **Standardwert** | first_name,last_name,company,description                                                                                           |

| **Konstante**    | themes.configuration.extension.address_manager.list.orderForAjaxResult                                                             |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Listenreihenfolge für Ajax-Ergebnis.                                                                                               |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | Auswahlbox mit Optionen: ,                                                                                                         |
| **Standardwert** |                                                                                                                                    |



## Adress-Listenansicht - karten

| **Konstante**    | themes.configuration.extension.address_manager.list.map.clustering.active                                                          |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Marker-Clustering aktivieren                                                                                                       |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | Auswahlbox mit Optionen: 0, 1                                                                                                      |
| **Standardwert** | 0                                                                                                                                  |

| **Konstante**    | themes.configuration.extension.address_manager.list.map.clustering.imagePath |
|:-----------------|:-----------------------------------------------------------------------------|
| **Label**        | Pfad für Clustering Bilder                                                   |
| **Beschreibung** |                                                                              |
| **Typ**          | string                                                                       |
| **Standardwert** | EXT:address_manager/Resources/Public/Images/MapClustering/m                  |

| **Konstante**    | themes.configuration.extension.address_manager.list.map.default.latitude                                                           |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Default Latitude                                                                                                                   |
| **Beschreibung** | Wird als Zentrum verwendet, wenn beim Laden keine Marker vorhanden sind                                                            |
| **Typ**          | string                                                                                                                             |
| **Standardwert** | 51.96066490000001                                                                                                                  |

| **Konstante**    | themes.configuration.extension.address_manager.list.map.default.longitude                                                          |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Default Longitude                                                                                                                  |
| **Beschreibung** | Wird als Zentrum verwendet, wenn beim Laden keine Marker vorhanden sind                                                            |
| **Typ**          | string                                                                                                                             |
| **Standardwert** | 7.626134699999966                                                                                                                  |

| **Konstante**    | themes.configuration.extension.address_manager.list.map.countryRestriction                                                         |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Ländereinschränkung für Karte                                                                                                      |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | string                                                                                                                             |
| **Standardwert** | de                                                                                                                                 |

| **Konstante**    | themes.configuration.extension.address_manager.list.map.maxZoom                                                                    |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Maximaler Zoom, der verwendet wird, indem das Ansichtsfenster auf die Markierungsgrenzen gesetzt wird                              |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | int+                                                                                                                               |
| **Standardwert** | 13                                                                                                                                 |

| **Konstante**    | themes.configuration.extension.address_manager.list.map.style                                                                      |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Optionen für Karte Styling                                                                                                         |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | Auswahlbox mit Optionen: , ,                                                                                                       |
| **Standardwert** |                                                                                                                                    |



## Adress-Detailansicht

| **Konstante**    | themes.configuration.extension.address_manager.detail.image.width                                                                  |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Detail Bildbreite                                                                                                                  |
| **Beschreibung** | Dieser Wert wird als maximum Wert benutzt.                                                                                         |
| **Typ**          | string                                                                                                                             |
| **Standardwert** | 600                                                                                                                                |

| **Konstante**    | themes.configuration.extension.address_manager.detail.image.height                                                                 |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Detail Bildhöhe                                                                                                                    |
| **Beschreibung** | Dieser Wert wird als maximum Wert benutzt.                                                                                         |
| **Typ**          | string                                                                                                                             |
| **Standardwert** | 400                                                                                                                                |

| **Konstante**    | themes.configuration.extension.address_manager.detail.map.zoom                                                                     |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Detail-Karte Zoom                                                                                                                  |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | int+                                                                                                                               |
| **Standardwert** | 17                                                                                                                                 |

| **Konstante**    | themes.configuration.extension.address_manager.detail.map.style                                                                    |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Detail-Karte Styling                                                                                                               |
| **Beschreibung** | Optionen für data-style                                                                                                            |
| **Typ**          | Auswahlbox mit Optionen: , ,                                                                                                       |
| **Standardwert** |                                                                                                                                    |



## Google Maps Zustimmung

| **Konstante**    | themes.configuration.extension.address_manager.googleMapsConsent.active                                                            |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Zeige Button und frage nach der Zustimmung, bevor Google Maps geladen wird                                                         |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | Auswahlbox mit Optionen: 0, 1                                                                                                      |
| **Standardwert** | 0                                                                                                                                  |



## Javascript

| **Konstante**    | themes.configuration.javascript.google.maps.apiKey                                                                                 |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Google Maps Api Schlüssel                                                                                                          |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | string                                                                                                                             |
| **Standardwert** |                                                                                                                                    |

| **Konstante**    | themes.configuration.javascript.google.maps.libraries                                                                              |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Google Maps Bibliotheken                                                                                                           |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | string                                                                                                                             |
| **Standardwert** | places                                                                                                                             |



## Framework

| **Konstante**    | themes.framework                                                                                                                   |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------|
| **Label**        | Framework                                                                                                                          |
| **Beschreibung** |                                                                                                                                    |
| **Typ**          | string                                                                                                                             |
| **Standardwert** | Bootstrap4                                                                                                                         |



