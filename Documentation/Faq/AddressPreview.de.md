# Warum funktioniert die Adressevorschau nicht?

Vielleicht hast Du vergessen die erforderliche Detail-Seite im TypoScript-Template zu definieren:

* Nach der Installation bitte zum Template Modul wechseln
* Den Konstanten-Editor in der Select-Box auswählen
* Danach die PAGES Kategorie in der Select-Box auswählen
* Dann "Seite für Adress-Details" mit der entsprechenden Uid der Detailseite versehen
