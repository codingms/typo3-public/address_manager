# Why does the address preview not work?

Maybe you've forgotten to add the required details page in your TypoScript-Template:

* After installation go to the Template Module
* Select Constant Editor from the Dropdown list
* Then select PAGES from the Category Dropdown list
* Under Address Pages, look for "Page for address details" and provide the correct Details page id.
