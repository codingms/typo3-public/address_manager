<?php

$iconList = [];
foreach (['ext-address-manager-icon1' => 'iconmonstr-eye-6.svg',
             'ext-address-manager-icon2' => 'iconmonstr-eye-8.svg',
             'ext-address-manager-icon3' => 'iconmonstr-id-card-8.svg',
             'ext-address-manager-icon4' => 'iconmonstr-link-2.svg',
             'ext-address-manager-icon5' => 'iconmonstr-location-3.svg',
             'ext-address-manager-icon6' => 'iconmonstr-location-pin-thin.svg',
             'ext-address-manager-icon7' => 'iconmonstr-map-8.svg',
             'ext-address-manager-icon8' => 'iconmonstr-map-9.svg',
             'ext-address-manager-icon9' => 'iconmonstr-undo-1.svg',
             'ext-address-manager-icon10' => 'module-address-manager.svg',
         ] as $identifier => $path) {
    $iconList[$identifier] = [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:address_manager/Resources/Public/Icons/' . $path,
    ];
}

return $iconList;
