<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

defined('TYPO3') or die();

if (ExtensionManagementUtility::isLoaded('calendars')) {
    $extKey = 'calendars';
    $table = 'tx_calendars_domain_model_calendarevent';
    $lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

    $newColumns = [
        'address' => [
            'exclude' => 0,
            'label' => $lll . '.address',
            'config' => [
                'type' => 'group',
                'allowed' => 'tx_addressmanager_domain_model_address',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 0,
                'default' => 0,
                'show_thumbs' => 1,
                'wizards' => [
                    'suggest' => [
                        'type' => 'suggest',
                    ],
                ],
            ],
        ],
    ];

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
        'tx_calendars_domain_model_calendarevent',
        $newColumns
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tx_calendars_domain_model_calendarevent',
        'address',
        '',
        'after:calendar_event_category'
    );
}
