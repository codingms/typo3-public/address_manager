<?php

return [
    'dependencies' => [
        'backend',
    ],
    'tags' => [
        'backend.form',
    ],
    'imports' => [
        '@codingms/address-manager/address-manager.js' => 'EXT:address_manager/Resources/Public/JavaScript/Backend/address-manager.js',
    ],
];
