# Address-Manager Erweiterung für TYPO3

Mit dem Adressmanager können Adressen bzw. Personendatensätze verwaltet und im Frontend dargestellt werden (vergleichbar mit tt_address).


**Features:**

*   Viele Datenfelder stehen zur Verfügung, neben Standardfeldern wie Name und Kontaktdaten bietet die Erweiterung spezielle Felder für SEO, Social Media, Geokoordinaten und Öffnungszeiten.
*   Umkreissuche, Filialfinder oder Händlersuche integriert.
*   Migration von tt_address möglich.
*   Bilder und Anhänge können verwendet werden.
*   Adressen können nach Gruppen, Organisationen und Positionen strukturiert werden.
*   Es steht eine Volltextsuche zur Verfügung.
*   Suchplugin kann an beliebiger Stelle und unabhängig von Liste/Karte positioniert werden.
*   Daten können nach diversen Kriterien gefiltert werden.
*   Diverse Filter können innerhalb der Suche in beliebiger Reihenfolge platziert werden.
*   Jede angelegte Adresse kann mittels entsprechender Felder suchmaschinenoptimiert werden.
*   Optional kann eine Detailseite je Adresse/Kontakt angezeigt werden.
*   Listen können einen Offset und ein Limit bekommen. So lassen sich auch einfach Teaser-Listen erstellen.
*   Anzeige von Google-Maps inkl. Marker je Standort.
*   Je nach ausgewähltem Filter positioniert sich die Google-Maps-Karte automatisch im Bereich der vorhandenen Marker.
*   Google Maps Marker-Clustering.
*   Ein ViewHelper ermöglicht die Anzeige von Adressen in anderen Fluid-Templates (wie Third-Party-Extensions oder Page-Templates).
*   Geokoordinaten können im TYPO3-Backend bei der Bearbeitung von Adressen ermittelt werden ("Kartenassistent").

**Pro-Features:**

*   Praktisches Backendmodul zur Pflege der Adress- bzw. Kontaktdaten inkl. erweiterter Autorisationen
*   Eine Frontend-Management-Funktion ermöglicht die Frontend-Bearbeitung von Adressen.
*   Ein Scheduler-Task wandelt automatisiert die Geo-Koordinaten in zugehörige Adressdaten um
*   Umkreissuche/Filialfinder

Wenn ein zusätzliches oder individuelles Feature benötigt wird - kontaktiere uns gern!


**Links:**

*   [Address-Manager Dokumentation](https://www.coding.ms/documentation/typo3-address-manager "Address-Manager Dokumentation")
*   [Address-Manager Bug-Tracker](https://gitlab.com/codingms/typo3-public/address_manager/-/issues "Address-Manager Bug-Tracker")
*   [Address-Manager Repository](https://gitlab.com/codingms/typo3-public/address_manager "Address-Manager Repository")
*   [TYPO3 Address-Manager Produktdetails](https://www.coding.ms/de/typo3-extensions/typo3-address-manager "TYPO3 Address-Manager Produktdetails")
*   [TYPO3 Address-Manager Dokumentation](https://www.coding.ms/de/dokumentation/typo3-address-manager "TYPO3 Address-Manager Dokumentation")
*   [TYPO3 Address-Manager Extension Download](https://extensions.typo3.org/extension/address_manager/ "TYPO3 Address-Manager Extension Download")
