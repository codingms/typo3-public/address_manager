<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'address_manager',
    'Addresslist',
    [
        \CodingMs\AddressManager\Controller\AddressController::class => 'list, show'
    ],
    // Don't cache list view, because of passed search parameters
    [
        \CodingMs\AddressManager\Controller\AddressController::class => 'list'
    ]
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'address_manager',
    'Addressgroupteaser',
    [
        \CodingMs\AddressManager\Controller\AddressController::class => 'groupTeaser'
    ],
    []
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'address_manager',
    'JsonApi',
    [
        \CodingMs\AddressManager\Controller\JsonApiController::class => 'select'
    ],
    [
        \CodingMs\AddressManager\Controller\JsonApiController::class => 'select'
    ]
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'address_manager',
    'Addresssingle',
    [
        \CodingMs\AddressManager\Controller\AddressController::class  => 'showSingle'
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'address_manager',
    'AddressSelectedList',
    [
        \CodingMs\AddressManager\Controller\AddressController::class => 'listSelected'
    ]
);
//
// Add new field type to NodeFactory in order to render the map_wizard
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1591191326] = [
    'nodeName' => 'geoLocationWizard',
    'priority' => '70',
    'class' => \CodingMs\AddressManager\Form\Element\GeoLocationElement::class,
];
//
// Add crtl wizard for checking valid address records
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1646045276] = [
    'nodeName' => 'ValidateAddressContainer',
    'priority' => '70',
    'class' => \CodingMs\AddressManager\Form\Container\ValidateAddressContainer::class,
];
//
// register svg icons: identifier and filename
$iconsSvg = [
    'module-address-manager' => 'Resources/Public/Icons/module-address-manager.svg',
    'contains-addresses' => 'Resources/Public/Icons/iconmonstr-id-card-8.svg',
    'apps-pagetree-folder-contains-addresses' => 'Resources/Public/Icons/iconmonstr-id-card-8.svg',
    'content-plugin-addressmanager-addresslist' => 'Resources/Public/Icons/iconmonstr-map-9.svg',
    'mimetypes-x-content-addressmanager-address' => 'Resources/Public/Icons/iconmonstr-id-card-8.svg',
    'mimetypes-x-content-addressmanager-addressgroup' => 'Resources/Public/Icons/iconmonstr-link-2.svg',
    'mimetypes-x-content-addressmanager-addressorganisation' => 'Resources/Public/Icons/iconmonstr-link-2.svg',
    'mimetypes-x-content-addressmanager-addressposition' => 'Resources/Public/Icons/iconmonstr-link-2.svg',
];
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
foreach ($iconsSvg as $identifier => $path) {
    $iconRegistry->registerIcon(
        $identifier,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:address_manager/' . $path]
    );
}
// Page TypoScript
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '@import "EXT:address_manager/Configuration/PageTS/tsconfig.typoscript"'
);
