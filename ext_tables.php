<?php

defined('TYPO3') || die('Access denied.');

call_user_func(
    function () {
        //
        // Table configuration arrays
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(
            'tx_addressmanager_domain_model_address'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(
            'tx_addressmanager_domain_model_addressgroup'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(
            'tx_addressmanager_domain_model_addressorganisation'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(
            'tx_addressmanager_domain_model_addressposition'
        );
    }
);
