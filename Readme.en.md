# Address-Manager Extension for TYPO3

The address manager extension allows you to manage and display addresses respectively person records (comparable to tt_address).


**Features:**

*   Lots of data fields are available, beside standard fields like name and contact information the extension provides special fields for SEO, social media, geo coordinates and opening hours.
*   Storefinder, radius search or radial search integrated
*   Migration of tt_address possible
*   Images and attachments can be used
*   Addresses may be structured using groups, organisations and positions.
*   A fulltext search is available.
*   Search plugin can be positioned anywhere on the list/map
*   Data can be filtered according to various criteria
*   Various filters can be added to the search in any order
*   Smooth presentation of lists and filters with AJAX.
*   Address fields allow search engine optimisation
*   Option to display detail page for each address/contact
*   Lists can have an offset and a limit allowing easy setup of teaser lists
*   Google Maps can be displayed, including location markers
*   Google Maps automatically locates existing marker areas based on selected filters
*   Google Maps marker clustering
*   A ViewHelper allows to display addresses in other fluid templates (like third-party-extensions or page templates).
*   Geo coordinates can be determined in the TYPO3 backend when addresses are edited ("map wizard").

**Pro-Features:**

*   Convenient backend module for maintaining address and contact details incl. extended authorizations
*   A frontend management function allows to edit addresses in the frontend.
*   A scheduler task to fetch geo coordinates automatically is included.
*   A radius search is included.

If you need some additional or custom feature - get in contact!


**Links:**

*   [Address-Manager Documentation](https://www.coding.ms/documentation/typo3-address-manager "Address-Manager Documentation")
*   [Address-Manager Bug-Tracker](https://gitlab.com/codingms/typo3-public/address_manager/-/issues "Address-Manager Bug-Tracker")
*   [Address-Manager Repository](https://gitlab.com/codingms/typo3-public/address_manager "Address-Manager Repository")
*   [TYPO3 Address-Manager Productdetails](https://www.coding.ms/typo3-extensions/typo3-address-manager "TYPO3 Address-Manager Productdetails")
*   [TYPO3 Address-Manager Documentation](https://www.coding.ms/documentation/typo3-address-manager "TYPO3 Address-Manager Documentation")
*   [TYPO3 Address-Manager Extension Download](https://extensions.typo3.org/extension/address_manager/ "TYPO3 Address-Manager Extension Download")
