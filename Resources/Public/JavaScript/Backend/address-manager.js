import DocumentService from "@typo3/core/document-service.js";
import jQuery from "jquery";
class AddressManager {

    /**
     * Container for the marker
     * @type {null}
     */
    marker = null;

    /**
     * Uid of the current record
     * @type {number}
     */
    uid = 0;

    /**
     * Container for google's geocoder
     * @type {null}
     */
    geocoder = null;

    /**
     * Container for google's geocoder
     * @type {null}
     */
    map = null;

    /**
     * Geo coordinates
     * @type {null}
     */
    latlng = null;

    initialize() {
        DocumentService.ready().then(() => {
            top.TYPO3.AddressManager = this;
            //
            top.TYPO3.AddressManager.map = jQuery('#address-manager-map');
            top.TYPO3.AddressManager.uid = parseInt(top.TYPO3.AddressManager.map.attr('data-uid'), 10);
            // Initialize geocoder
            top.TYPO3.AddressManager.geocoder = new google.maps.Geocoder();
            //
            let latitude = 51.9637546;
            let longitude = 7.6081952;
            top.TYPO3.AddressManager.latlng = new google.maps.LatLng(latitude, longitude);
            //
            // Create map
            top.TYPO3.AddressManager.map = new google.maps.Map(document.getElementById('address-manager-map'), {
                // Initial zoom
                zoom: 12,
                center: top.TYPO3.AddressManager.latlng,
                zoomControl: true,
                scaleControl: true,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            //
            // Update marker
            this.updateMarker(latitude, longitude);
            //
            // Bind events on buttons
            jQuery.each(jQuery('#address-manager-input *[data-event]'), function () {
                let button = jQuery(this);
                let event = button.attr('data-event');
                switch (event) {
                    case 'grabAddressFromRecord':
                        button.on('click', top.TYPO3.AddressManager.grabAddressFromRecord);
                        break;
                    case 'getGeoLocation':
                        button.on('click', top.TYPO3.AddressManager.getGeoLocation);
                        break;
                    case 'showMap':
                        button.on('click', top.TYPO3.AddressManager.showMap);
                        break;
                }
            });
            //
            // Fetching on enter on search field
            let fieldIdentifier = 'data[tx_addressmanager_domain_model_address][' + top.TYPO3.AddressManager.uid + '][map_wizard]';
            jQuery('[data-formengine-input-name="' + fieldIdentifier + '"]').keypress(function(event){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13') {
                    top.TYPO3.AddressManager.getGeoLocation();
                    event.preventDefault();
                    return false;
                }
            });
        });
    };

    /**
     * Show the current geo location on map
     */
    showMap = function () {
        jQuery('#address-manager-map').show();
        let latitude = parseFloat(top.TYPO3.AddressManager.getFieldValue('map_latitude'));
        let longitude = parseFloat(top.TYPO3.AddressManager.getFieldValue('map_longitude'));
        top.TYPO3.AddressManager.updateMarker(latitude, longitude);
    };

    /**
     * Grab geo location from Google-Maps API
     */
    getGeoLocation = function () {
        let addressMap = jQuery('#address-manager-map');
        addressMap.show();
        let address = top.TYPO3.AddressManager.getFieldValue('map_wizard');
        if (address !== '') {
            top.TYPO3.AddressManager.geocoder.geocode({address: address}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    let latlng = results[0].geometry.location;
                    top.TYPO3.AddressManager.setFieldValue('map_latitude', latlng.lat());
                    top.TYPO3.AddressManager.setFieldValue('map_longitude', latlng.lng());
                    top.TYPO3.AddressManager.updateMarker(latlng.lat(), latlng.lng());
                    top.TYPO3.AddressManager.clearMessage();
                } else if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
                    let message = addressMap.attr('data-message-address-not-found');
                    top.TYPO3.AddressManager.showMessage(message + ': ' + address, 'info');
                } else {
                    let message = addressMap.attr('data-message-geocoder-status');
                    top.TYPO3.AddressManager.showMessage(message + ': ' + status, 'danger');
                }
            });
        } else {
            let message = addressMap.attr('data-message-enter-an-address');
            top.TYPO3.AddressManager.showMessage(message, 'danger');
        }
    };

    /**
     * Update marker
     *
     * @param latitude
     * @param longitude
     */
    updateMarker = function (latitude, longitude) {
        top.TYPO3.AddressManager.latlng = new google.maps.LatLng(latitude, longitude);
        // Reset marker
        if (top.TYPO3.AddressManager.marker) {
            top.TYPO3.AddressManager.marker.setMap(null);
        }
        // Set new marker
        top.TYPO3.AddressManager.marker = new google.maps.Marker({
            map: top.TYPO3.AddressManager.map,
            position: top.TYPO3.AddressManager.latlng,
            draggable: true
        });
        // Set the center of the map
        top.TYPO3.AddressManager.map.setCenter(top.TYPO3.AddressManager.latlng);
        // Add event listener for marker-repositioning
        google.maps.event.addListener(top.TYPO3.AddressManager.marker, "dragend", function () {
            let latitude = top.TYPO3.AddressManager.marker.getPosition().lat();
            let longitude = top.TYPO3.AddressManager.marker.getPosition().lng();
            top.TYPO3.AddressManager.setFieldValue('map_latitude', latitude);
            top.TYPO3.AddressManager.setFieldValue('map_longitude', longitude);
            top.TYPO3.AddressManager.setFieldValue('map_wizard', '');
            top.TYPO3.AddressManager.clearMessage();
        });
        setTimeout(function () {
            google.maps.event.trigger(top.TYPO3.AddressManager.map, 'resize');
            setTimeout(function () {
                top.TYPO3.AddressManager.map.setCenter(top.TYPO3.AddressManager.latlng);
            }, 500);
        }, 500);
    };

    /**
     * Build address string by record fields
     */
    grabAddressFromRecord = function () {
        let addressParts = [];
        // Country available?
        let country = top.TYPO3.AddressManager.getFieldValue('country');
        if (typeof country === 'string' && country !== '') {
            addressParts.push(country + ',');
        }
        // Region available?
        let region = top.TYPO3.AddressManager.getFieldValue('region');
        if (typeof region === 'string' && region !== '') {
            addressParts.push(region + ',');
        }
        // Postal code available?
        let postalCode = top.TYPO3.AddressManager.getFieldValue('postalCode');
        if (typeof postalCode === 'string' && postalCode !== '') {
            addressParts.push(postalCode);
        }
        // City available?
        let city = top.TYPO3.AddressManager.getFieldValue('city');
        if (typeof city === 'string' && city !== '') {
            addressParts.push(city);
        }
        // Address available?
        let address = top.TYPO3.AddressManager.getFieldValue('address');
        if (typeof address === 'string' && address !== '') {
            addressParts.push(address);
        }
        top.TYPO3.AddressManager.setFieldValue('map_wizard', addressParts.join(' '));
        top.TYPO3.AddressManager.clearMessage();
    };

    /**
     * Get field value
     *
     * @param field
     * @returns string
     */
    getFieldValue = function (field) {
        let value = '';
        let fieldObject = TYPO3.FormEngine.getFieldElement(
            'data[tx_addressmanager_domain_model_address][' + top.TYPO3.AddressManager.uid + '][' + field + ']'
        );
        if (fieldObject.length > 0) {
            value = fieldObject.val();
        }
        return value;
    };

    /**
     * Set field value
     *
     * @param field
     * @param value
     */
    setFieldValue = function (field, value) {
        let fieldIdentifier = 'data[tx_addressmanager_domain_model_address][' + top.TYPO3.AddressManager.uid + '][' + field + ']';
        let formField = jQuery('[data-formengine-input-name="' + fieldIdentifier + '"]');
        let fieldObject = TYPO3.FormEngine.getFieldElement(fieldIdentifier);
        fieldObject.val(value);
        formField.val(value);
        //
        // Mark palette as changed
        let paletteField = formField.closest('.t3js-formengine-palette-field');
        paletteField.addClass('has-change');
    };

    /**
     * Show a message
     *
     * @param message Message text
     * @param status Valid values: info, danger, success
     */
    showMessage = function (message, status) {
        jQuery('#address-manager-message')
            .html(jQuery('<span />', {'class': 'text-' + status}).text(message))
            .show();
    };

    /**
     * Clear message
     */
    clearMessage = function () {
        jQuery('#address-manager-message').html('');
    };
}

export default new AddressManager();
