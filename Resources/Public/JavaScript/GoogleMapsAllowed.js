var AddressManagerGoogleMapsConsent = function () {
    this.cookieSet = function(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }
    this.cookieGet = function(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }
    this.showGoogleConsentButton = function() {
        if (jQuery('.map-placeholder')) {
            let googleMapsPlaceholder = jQuery('.map-placeholder').html();
            jQuery('.map-placeholder').html('')
            let googleMapsAllowed = this.cookieGet('googleMapsAllowed');
            if (googleMapsAllowed === null) {
                let mapWrapperAddressList = jQuery('#map-wrapper');
                if (mapWrapperAddressList.length > 0) {
                    mapWrapperAddressList.html(googleMapsPlaceholder);
                }
            }
        }
    }
    this.activateGoogleMaps = function() {
        let googleMapsCheckbox = jQuery('#map-privacy-check');
        if (googleMapsCheckbox.is(":checked")) {
            this.cookieSet('googleMapsAllowed', '1', 365);
            window.location.reload(false);
        } else {
            googleMapsCheckbox.addClass('is-invalid');
        }
    }
}
let address_manager_google_maps_consent = new AddressManagerGoogleMapsConsent();
jQuery(document).ready(function () {
    address_manager_google_maps_consent.showGoogleConsentButton();
});
