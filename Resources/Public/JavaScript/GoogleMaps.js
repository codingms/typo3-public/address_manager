//
// Google maps
// Example:
// <div class="google-map" data-title="Radical.Design 3"
// data-latitude="51.96464" data-longitude="7.561069999999972"
// data-zoom="12" data-style="primaryColored"
// data-description="Strasse<br />33333 Ort"
// data-open-tooltip="immediately"></div>
//
// Options for data-style:
// primaryColored: Colored in theme primary color
// normal: Googles default colors
// grayScaled: Gray scaled
//
// Options for data-open-tooltip:
// immediately: Show tooltip immediately on startup
// onclick: Show tooltip on click
jQuery.each(jQuery('.google-map'), function(key) {
	var object = jQuery(this);
	var zoom = parseInt(object.attr('data-zoom'), 10);
	var title = object.attr('data-title');
	var description = object.attr('data-description');
	var latitude = object.attr('data-latitude');
	var longitude = object.attr('data-longitude');
	var openTooltip = object.attr('data-open-tooltip');
	// Map id
	var id = 'map-' + key;
	object.attr('id', id);
	// Primary color
	var color = jQuery('body').attr('data-color-primary');
	if(typeof color === 'undefined') {
		color = '#ddb0b7';
	}
	// Map style
	var style = object.attr('data-style');
	// Default styles
	// Gray scale
	var mapStyles = [
		{
			featureType: 'all',
			stylers: [
				{saturation: -100},
				{gamma: 0.90}
			]
		}
	];
	if(style === 'primaryColored') {
		mapStyles = [
			{
				stylers: [
					{hue: color},
					{saturation: -70},
					{lightness: 30},
					{gamma: 1}
				]
			}
		];
	}
	else if (style === 'normal') {
		mapStyles = [];
	}
	//
	var latlng = new google.maps.LatLng(latitude, longitude);
	var myOptions = {
		zoom: zoom,
		center: latlng,
		zoomControl: true,
		scaleControl: true,
		scrollwheel: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById(id), myOptions);
	map.setOptions({styles: mapStyles});
	//
	// Set marker


	var width = parseInt(object.attr('data-marker-width'), 10);
	var height = parseInt(object.attr('data-marker-height'), 10);
	var svg = object.attr('data-marker-svg');
	var icon = {
		anchor: new google.maps.Point((width/2), height),
		scaledSize: new google.maps.Size(width, height),
		origin: new google.maps.Point(0,0),
		url: 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(svg)
	};
	var marker = new google.maps.Marker({
		position: latlng,
		icon: icon,
		map: map
	});
	/*
	var marker = new HtmlMarker(latlng, map, {
		class: 'google-map-marker',
		id: id + '-marker',
		innerHTML: '<span class="icon icon-pin icon-lg" title="' + title + '"><div class="information">' + title + '<br />' + description + '</div></span>',
		position: 'CT',
		title: title
	});
	*/
	// Show name
	google.maps.event.addListener(marker, 'click', function () {
		jQuery('#' + this.args.id + ' .information').fadeIn();
	});
	if(openTooltip === 'immediately') {
		setTimeout(function() {
			jQuery('#' + id + ' .information').fadeIn();
		}, 1000);
	}
	// Hide name
	google.maps.event.addListener(marker, 'mouseout', function () {
		//var marker = jQuery('#' + this.args.id + ' .information').fadeOut();
	});
});
