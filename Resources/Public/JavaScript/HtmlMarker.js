function HtmlMarker(latlng, map, args) {
	this.latlng = latlng;
	this.args = args;
	this.setMap(map);
}
HtmlMarker.prototype = new google.maps.OverlayView();
HtmlMarker.prototype.draw = function() {
	var self = this;
	var div = this.div;
	if (!div) {
		div = this.div = document.createElement('div');
		div.id = this.args.id;
		div.className = this.args.class;
		div.innerHTML = this.args.innerHTML;
		if (typeof(self.args.marker_id) !== 'undefined') {
			div.dataset.marker_id = self.args.marker_id;
		}
		google.maps.event.addDomListener(div, "click", function(event) {
			google.maps.event.trigger(self, "click");
		});
		google.maps.event.addDomListener(div, "mouseover", function(event) {
			google.maps.event.trigger(self, "mouseover");
		});
		google.maps.event.addDomListener(div, "mouseout", function(event) {
			google.maps.event.trigger(self, "mouseout");
		});
		var panes = this.getPanes();
		panes.overlayImage.appendChild(div);
	}
	this.setPosition(this.latlng);
};
HtmlMarker.prototype.remove = function() {
	if (this.div) {
		this.div.parentNode.removeChild(this.div);
		this.div = null;
	}
};
HtmlMarker.prototype.getPosition = function() {
	return this.latlng;
};
HtmlMarker.prototype.setPosition = function(latlng) {
	var point = this.getProjection().fromLatLngToDivPixel(latlng);
	if (point) {
		var marker = jQuery('#' + this.args.id);
		var div = marker.closest('div');
		// Attention:
		// Marker-CSS must contain a negative margin!
		if(this.args.position === 'CC') {
			div.css('left', (point.x - parseInt(marker.css('width'), 10)/2) + 'px');
			div.css('top', (point.y - parseInt(marker.css('height'), 10)/2) + 'px');
		}
		else if(this.args.position === 'CT') {
			div.css('left', point.x + 'px');
			div.css('top', point.y + 'px');
		}
	}
};
