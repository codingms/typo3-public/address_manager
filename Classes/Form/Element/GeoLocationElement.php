<?php

namespace CodingMs\AddressManager\Form\Element;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Backend\Form\Element\AbstractFormElement;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Page\JavaScriptModuleInstruction;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;

use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;

/**
 * Generation of TCEform elements of the type "input type=text"
 *
 * Note: This form engine element was inspired initially by EXT:geolocations
 * by Stephan Kellermayr / sonority.at - MULTIMEDIA ART DESIGN
 */
class GeoLocationElement extends AbstractFormElement
{

    /**
     * @var ConfigurationManager
     */
    protected ConfigurationManager $configurationManager;

    /**
     * @var array<mixed>
     */
    protected array $extensionConfiguration = [];

    /**
     * This will render a single-line input form field, possibly with various control/validation features
     *
     * @return array<mixed>
     * @throws ExtensionConfigurationExtensionNotConfiguredException
     * @throws ExtensionConfigurationPathDoesNotExistException
     */
    public function render(): array
    {
        //
        $extensionKey = 'address_manager';
        $parameterArray = $this->data['parameterArray'] ?? [];
        //
        // Get extension configuration
        $this->fetchExtensionConfiguration($extensionKey);
        //
        // Google maps
        $apiKey = $this->getGoogleMapsApiKey();
        $message = '';
        if ($apiKey === '') {
            $message = '<span class="text-danger">' . $this->translate('api_key_not_found') . '</span>';
        }
        //
        $mapsUrl = '//maps.google.com/maps/api/js?key=' . $apiKey . '&libraries=places';
                    //
        // Add JavaScript and CSS
        /** @var PageRenderer $pageRenderer */
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $pageRenderer->addJsFile($mapsUrl, null, false, true, '', true);
        $extRealPath = '../' . PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($extensionKey));

        if((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
            $pageRenderer->loadRequireJsModule('TYPO3/CMS/AddressManager/Backend/AddressManager');
            $pageRenderer->addCssFile($extRealPath . 'Resources/Public/Stylesheets/Backend/AddressManager.css');
        } else {
            $pageRenderer->getJavaScriptRenderer()->addJavaScriptModuleInstruction(
                JavaScriptModuleInstruction::create('@codingms/address-manager/address-manager.js')->invoke('initialize')
            );
            $pageRenderer->addCssFile( 'EXT:address_manager/Resources/Public/Stylesheets/Backend/AddressManager.css');
        }

        $resultArray = [
            'additionalJavaScriptPost' => [],
            'additionalJavaScriptSubmit' => [],
            'additionalHiddenFields' => [],
            'additionalInlineLanguageLabelFiles' => [],
            'stylesheetFiles' => [],
            // Can hold strings or arrays, string = requireJS module, array = requireJS module + callback e.g. array('TYPO3/Foo/Bar', 'function() {}')
            'requireJsModules' => [],
            'extJSCODE' => '',
            'inlineData' => []
        ];

        $config = $parameterArray['fieldConf']['config'] ?? [];
        $classes = [];
        $attributes = [];
        $validationRules = [];
        $paramsList = [
            'field' => $parameterArray['itemFormElName'] ?? '',
            'evalList' => '',
            'is_in' => '',
        ];
        //
        // Set classes
        $classes[] = 'form-control';
        $classes[] = 't3js-clearable';
        $classes[] = 'hasDefaultValue';
        //
        // Calculate attributes
        $attributes['data-formengine-validation-rules'] = json_encode($validationRules);
        $attributes['data-formengine-input-params'] = json_encode($paramsList);
        $attributes['data-formengine-input-name'] = htmlspecialchars($parameterArray['itemFormElName'] ?? '');
        $attributes['id'] = StringUtility::getUniqueId('formengine-input-');
        $attributes['value'] = '';
        if (!empty($config['max'])) {
            $attributes['maxlength'] = (int)$config['max'];
        }

            $attributes['class'] = implode(' ', $classes);

        //
        // This is the EDITABLE form field.
        if (!empty($config['placeholder'])) {
            $attributes['placeholder'] = trim($config['placeholder']);
        }
        //
        // Build the attribute string
        $attributeString = '';
        foreach ($attributes as $attributeName => $attributeValue) {
            $attributeString .= ' ' . $attributeName . '="' . htmlspecialchars((string)$attributeValue) . '"';
        }
        //
        // Get translations
        $grabAddressFromRecordTitle = $this->translate('grab_address_from_record');
        $getGeoLocationTitle = $this->translate('get_geo_location');
        $showMapTitle = $this->translate('show_on_map');
        $addressNotFound = $this->translate('address_not_found');
        $geocoderStatus = $this->translate('geocoder_status');
        $enterAnAddress = $this->translate('enter_an_address');
        //
        $elementName = $parameterArray['itemFormElName'] ?? '';
        $elementValue = htmlspecialchars($parameterArray['itemFormElValue'] ?? '');
        //
        $iconFactory = GeneralUtility::makeInstance(IconFactory::class);
        /** @var Icon $icon1 */
        $icon1 = $iconFactory->getIcon(
            'ext-address-manager-icon9',
            Icon::SIZE_SMALL,
            'overlay-identifier'
        );
        /** @var Icon $icon2 */
        $icon2 = $iconFactory->getIcon(
            'ext-address-manager-icon7',
            Icon::SIZE_SMALL,
            'overlay-identifier'
        );
        /** @var Icon $icon3 */
        $icon3 = $iconFactory->getIcon(
            'ext-address-manager-icon1',
            Icon::SIZE_SMALL,
            'overlay-identifier'
        );
        $img1 = $icon1->getMarkup();
        $img2 = $icon2->getMarkup();
        $img3 = $icon3->getMarkup();
        //
        if((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
            $iconPath = '/typo3conf/ext/' . $extensionKey . '/Resources/Public/Icons/';
            $img1 = '<img width="16" height="16" src="' . $iconPath . 'iconmonstr-undo-1.svg" alt="' . $grabAddressFromRecordTitle . '">';
            $img2 = '<img width="16" height="16" src="' . $iconPath . 'iconmonstr-map-8.svg" alt="' . $getGeoLocationTitle . '">';
            $img3 = '<img width="16" height="16" src="' . $iconPath . 'iconmonstr-eye-6.svg" alt="' . $showMapTitle . '">';
        }
        //
        // Build HTML
        $html = '
            <div class="form-control-wrap">
                <div class="input-group" id="address-manager-input">
                    <input type="text" ' . $attributeString . ' />
                    <input type="hidden" name="' . $elementName . '" value="' . $elementValue . '" />
                    <span class="input-group-btn m-0" data-event="grabAddressFromRecord">
                        <label class="btn btn-default" title="' . $grabAddressFromRecordTitle . '">
                            '.$img1.'
                        </label>
                    </span>
                    <span class="input-group-btn m-0" data-event="getGeoLocation">
                        <label class="btn btn-default" title="' . $getGeoLocationTitle . '">
                            '.$img2.'
                        </label>
                    </span>
                    <span class="input-group-btn" data-event="showMap">
                        <label class="btn btn-default" title="' . $showMapTitle . '">
                            '.$img3.'
                        </label>
                    </span>
                </div>
                <div id="address-manager-message">' . $message . '</div>
            </div>
            <div id="address-manager-map" data-uid="' . ($this->data['databaseRow']['uid'] ?? 0) . '" data-message-address-not-found="' . $addressNotFound . '" data-message-geocoder-status="' . $geocoderStatus . '" data-message-enter-an-address="' . $enterAnAddress . '"></div>
            ';
        //
        $resultArray['html'] = $html;
        return $resultArray;
    }

    /**
     * @param string $extensionKey
     * @return void
     * @throws ExtensionConfigurationExtensionNotConfiguredException
     * @throws ExtensionConfigurationPathDoesNotExistException
     */
    protected function fetchExtensionConfiguration(string $extensionKey)
    {
        /** @var ExtensionConfiguration $extensionConfigurationAPI */
        $extensionConfigurationAPI = GeneralUtility::makeInstance(ExtensionConfiguration::class);
        $this->extensionConfiguration = $extensionConfigurationAPI->get($extensionKey);
    }

    /**
     * @return string
     */
    protected function getGoogleMapsApiKey()
    {
        $apiKey = '';
        if (!empty($this->extensionConfiguration['googleMaps']['apiKey'])) {
            $apiKey = trim($this->extensionConfiguration['googleMaps']['apiKey']);
        }
        return $apiKey;
    }

    /**
     * @return LanguageService
     */
    protected function getLanguageService(): LanguageService
    {
        return $GLOBALS['LANG'];
    }

    /**
     * @param string $key
     * @return string
     */
    protected function translate(string $key): string
    {
        $translationKey = 'LLL:EXT:address_manager/Resources/Private/Language/locallang_db.xlf';
        $translationKey .= ':tx_addressmanager_domain_model_address.map_marker_wizard_' . $key;
        $translation = $this->getLanguageService()->sL($translationKey);
        return $translation;
    }
}
