<?php

namespace CodingMs\AddressManager\Form\Container;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Backend\Form\Container\AbstractContainer;

/**
 * Validate address record
 */
class ValidateAddressContainer extends AbstractContainer
{
    /**
     * @return array<string, mixed>
     */
    public function render()
    {
        $row = $this->data['databaseRow'] ?? [];
        $resultArray = $this->initializeResultArray();
        $messages = [];
        $tcaColumns = $this->data['processedTca']['columns'] ?? [];
        if (isset($row['groups']) &&
            is_array($row['groups'])
            && empty($row['groups'])
            && !empty($tcaColumns['groups']['config']['items'])) {
            $messages[] = 'There are groups available, but no groups selected!';
        }
        if (isset($row['positions']) &&
            is_array($row['positions'])
            && empty($row['positions'])
            && !empty($tcaColumns['positions']['config']['items'])) {
            $messages[] = 'There are positions available, but no positions selected!';
        }
        if (isset($row['organisations']) &&
            is_array($row['organisations'])
            && empty($row['organisations'])
            && !empty($tcaColumns['organisations']['config']['items'])) {
            $messages[] = 'There are organisations available, but no organisations selected!';
        }
        $resultArray['html'] = '';
        if (count($messages)) {
            $resultArray['html'] = '<div class="alert alert-warning">';
            $resultArray['html'] .= '<p>' . implode('<br />', $messages) . '</p>';
            $resultArray['html'] .= '<p><b>Please select at least one of those relation!</b></p>';
            $resultArray['html'] .= '</div>';
        }
        return $resultArray;
    }
}
