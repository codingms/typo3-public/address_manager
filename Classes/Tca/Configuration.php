<?php

declare(strict_types=1);

namespace CodingMs\AddressManager\Tca;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 **************************************************************/

use TYPO3\CMS\Core\Utility\VersionNumberUtility;

/**
 * Configuration presets for TCA fields.
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class Configuration extends \CodingMs\AdditionalTca\Tca\Configuration
{
    /**
     * @param string $type
     * @param bool $required
     * @param bool $readonly
     * @param string $label
     * @param array<mixed> $options
     * @return array<string, mixed>
     */
    public static function get($type, $required = false, $readonly = false, $label = '', array $options=[]): array
    {
        $typo3Version = (int)VersionNumberUtility::getCurrentTypo3Version();
        switch ($type) {
            case 'groupVariant':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'default' => 'default',
                    'items' => [
                        ['label' => 'default', 'value' => 'default'],
                        ['label' => 'primary', 'value' => 'primary'],
                        ['label' => 'secondary', 'value' => 'secondary'],
                        ['label' => 'success', 'value' => 'success'],
                        ['label' => 'info', 'value' => 'info'],
                        ['label' => 'warning', 'value' => 'warning'],
                        ['label' => 'danger', 'value' => 'danger'],
                    ],
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                ];
                if ($typo3Version < 12) {
                    $config['items'] = [
                        ['default', 'default'],
                        ['primary', 'primary'],
                        ['secondary', 'secondary'],
                        ['success', 'success'],
                        ['info', 'info'],
                        ['warning', 'warning'],
                        ['danger', 'danger'],
                    ];
                }
                break;
            case 'openingHours':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'default' => 'closed',
                    'items' => [
                        ['label' => 'closed', 'value' => 'closed'],
                        ['label' => '00:00', 'value' => '00:00'],
                        ['label' => '00:15', 'value' => '00:15'],
                        ['label' => '00:30', 'value' => '00:30'],
                        ['label' => '00:45', 'value' => '00:45'],
                        ['label' => '01:00', 'value' => '01:00'],
                        ['label' => '01:15', 'value' => '01:15'],
                        ['label' => '01:30', 'value' => '01:30'],
                        ['label' => '01:45', 'value' => '01:45'],
                        ['label' => '02:00', 'value' => '02:00'],
                        ['label' => '02:15', 'value' => '02:15'],
                        ['label' => '02:30', 'value' => '02:30'],
                        ['label' => '02:45', 'value' => '02:45'],
                        ['label' => '03:00', 'value' => '03:00'],
                        ['label' => '03:15', 'value' => '03:15'],
                        ['label' => '03:30', 'value' => '03:30'],
                        ['label' => '03:45', 'value' => '03:45'],
                        ['label' => '04:00', 'value' => '04:00'],
                        ['label' => '04:15', 'value' => '04:15'],
                        ['label' => '04:30', 'value' => '04:30'],
                        ['label' => '04:45', 'value' => '04:45'],
                        ['label' => '05:00', 'value' => '05:00'],
                        ['label' => '05:15', 'value' => '05:15'],
                        ['label' => '05:30', 'value' => '05:30'],
                        ['label' => '05:45', 'value' => '05:45'],
                        ['label' => '06:00', 'value' => '06:00'],
                        ['label' => '06:15', 'value' => '06:15'],
                        ['label' => '06:30', 'value' => '06:30'],
                        ['label' => '06:45', 'value' => '06:45'],
                        ['label' => '07:00', 'value' => '07:00'],
                        ['label' => '07:15', 'value' => '07:15'],
                        ['label' => '07:30', 'value' => '07:30'],
                        ['label' => '07:45', 'value' => '07:45'],
                        ['label' => '08:00', 'value' => '08:00'],
                        ['label' => '08:15', 'value' => '08:15'],
                        ['label' => '08:30', 'value' => '08:30'],
                        ['label' => '08:45', 'value' => '08:45'],
                        ['label' => '09:00', 'value' => '09:00'],
                        ['label' => '09:15', 'value' => '09:15'],
                        ['label' => '09:30', 'value' => '09:30'],
                        ['label' => '09:45', 'value' => '09:45'],
                        ['label' => '10:00', 'value' => '10:00'],
                        ['label' => '10:15', 'value' => '10:15'],
                        ['label' => '10:30', 'value' => '10:30'],
                        ['label' => '10:45', 'value' => '10:45'],
                        ['label' => '11:00', 'value' => '11:00'],
                        ['label' => '11:15', 'value' => '11:15'],
                        ['label' => '11:30', 'value' => '11:30'],
                        ['label' => '11:45', 'value' => '11:45'],
                        ['label' => '12:00', 'value' => '12:00'],
                        ['label' => '12:15', 'value' => '12:15'],
                        ['label' => '12:30', 'value' => '12:30'],
                        ['label' => '12:45', 'value' => '12:45'],
                        ['label' => '13:00', 'value' => '13:00'],
                        ['label' => '13:15', 'value' => '13:15'],
                        ['label' => '13:30', 'value' => '13:30'],
                        ['label' => '13:45', 'value' => '13:45'],
                        ['label' => '14:00', 'value' => '14:00'],
                        ['label' => '14:15', 'value' => '14:15'],
                        ['label' => '14:30', 'value' => '14:30'],
                        ['label' => '14:45', 'value' => '14:45'],
                        ['label' => '15:00', 'value' => '15:00'],
                        ['label' => '15:15', 'value' => '15:15'],
                        ['label' => '15:30', 'value' => '15:30'],
                        ['label' => '15:45', 'value' => '15:45'],
                        ['label' => '16:00', 'value' => '16:00'],
                        ['label' => '16:15', 'value' => '16:15'],
                        ['label' => '16:30', 'value' => '16:30'],
                        ['label' => '16:45', 'value' => '16:45'],
                        ['label' => '17:00', 'value' => '17:00'],
                        ['label' => '17:15', 'value' => '17:15'],
                        ['label' => '17:30', 'value' => '17:30'],
                        ['label' => '17:45', 'value' => '17:45'],
                        ['label' => '18:00', 'value' => '18:00'],
                        ['label' => '18:15', 'value' => '18:15'],
                        ['label' => '18:30', 'value' => '18:30'],
                        ['label' => '18:45', 'value' => '18:45'],
                        ['label' => '19:00', 'value' => '19:00'],
                        ['label' => '19:15', 'value' => '19:15'],
                        ['label' => '19:30', 'value' => '19:30'],
                        ['label' => '19:45', 'value' => '19:45'],
                        ['label' => '20:00', 'value' => '20:00'],
                        ['label' => '20:15', 'value' => '20:15'],
                        ['label' => '20:30', 'value' => '20:30'],
                        ['label' => '20:45', 'value' => '20:45'],
                        ['label' => '21:00', 'value' => '21:00'],
                        ['label' => '21:15', 'value' => '21:15'],
                        ['label' => '21:30', 'value' => '21:30'],
                        ['label' => '21:45', 'value' => '21:45'],
                        ['label' => '22:00', 'value' => '22:00'],
                        ['label' => '22:15', 'value' => '22:15'],
                        ['label' => '22:30', 'value' => '22:30'],
                        ['label' => '22:45', 'value' => '22:45'],
                        ['label' => '23:00', 'value' => '23:00'],
                        ['label' => '23:15', 'value' => '23:15'],
                        ['label' => '23:30', 'value' => '23:30'],
                        ['label' => '23:45', 'value' => '23:45'],
                    ],
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                ];
                if ($typo3Version < 12) {
                    $config['items'] = [
                        ['closed', 'closed'],
                        ['00:00', '00:00'],
                        ['00:15', '00:15'],
                        ['00:30', '00:30'],
                        ['00:45', '00:45'],
                        ['01:00', '01:00'],
                        ['01:15', '01:15'],
                        ['01:30', '01:30'],
                        ['01:45', '01:45'],
                        ['02:00', '02:00'],
                        ['02:15', '02:15'],
                        ['02:30', '02:30'],
                        ['02:45', '02:45'],
                        ['03:00', '03:00'],
                        ['03:15', '03:15'],
                        ['03:30', '03:30'],
                        ['03:45', '03:45'],
                        ['04:00', '04:00'],
                        ['04:15', '04:15'],
                        ['04:30', '04:30'],
                        ['04:45', '04:45'],
                        ['05:00', '05:00'],
                        ['05:15', '05:15'],
                        ['05:30', '05:30'],
                        ['05:45', '05:45'],
                        ['06:00', '06:00'],
                        ['06:15', '06:15'],
                        ['06:30', '06:30'],
                        ['06:45', '06:45'],
                        ['07:00', '07:00'],
                        ['07:15', '07:15'],
                        ['07:30', '07:30'],
                        ['07:45', '07:45'],
                        ['08:00', '08:00'],
                        ['08:15', '08:15'],
                        ['08:30', '08:30'],
                        ['08:45', '08:45'],
                        ['09:00', '09:00'],
                        ['09:15', '09:15'],
                        ['09:30', '09:30'],
                        ['09:45', '09:45'],
                        ['10:00', '10:00'],
                        ['10:15', '10:15'],
                        ['10:30', '10:30'],
                        ['10:45', '10:45'],
                        ['11:00', '11:00'],
                        ['11:15', '11:15'],
                        ['11:30', '11:30'],
                        ['11:45', '11:45'],
                        ['12:00', '12:00'],
                        ['12:15', '12:15'],
                        ['12:30', '12:30'],
                        ['12:45', '12:45'],
                        ['13:00', '13:00'],
                        ['13:15', '13:15'],
                        ['13:30', '13:30'],
                        ['13:45', '13:45'],
                        ['14:00', '14:00'],
                        ['14:15', '14:15'],
                        ['14:30', '14:30'],
                        ['14:45', '14:45'],
                        ['15:00', '15:00'],
                        ['15:15', '15:15'],
                        ['15:30', '15:30'],
                        ['15:45', '15:45'],
                        ['16:00', '16:00'],
                        ['16:15', '16:15'],
                        ['16:30', '16:30'],
                        ['16:45', '16:45'],
                        ['17:00', '17:00'],
                        ['17:15', '17:15'],
                        ['17:30', '17:30'],
                        ['17:45', '17:45'],
                        ['18:00', '18:00'],
                        ['18:15', '18:15'],
                        ['18:30', '18:30'],
                        ['18:45', '18:45'],
                        ['19:00', '19:00'],
                        ['19:15', '19:15'],
                        ['19:30', '19:30'],
                        ['19:45', '19:45'],
                        ['20:00', '20:00'],
                        ['20:15', '20:15'],
                        ['20:30', '20:30'],
                        ['20:45', '20:45'],
                        ['21:00', '21:00'],
                        ['21:15', '21:15'],
                        ['21:30', '21:30'],
                        ['21:45', '21:45'],
                        ['22:00', '22:00'],
                        ['22:15', '22:15'],
                        ['22:30', '22:30'],
                        ['22:45', '22:45'],
                        ['23:00', '23:00'],
                        ['23:15', '23:15'],
                        ['23:30', '23:30'],
                        ['23:45', '23:45'],
                    ];
                }
                break;
            default:
                $config = parent::get($type, $required, $readonly, $label, $options);
                break;
        }
        if ($readonly) {
            $config['readOnly'] = true;
        }
        return $config;
    }
}
