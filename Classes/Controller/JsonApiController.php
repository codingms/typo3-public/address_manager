<?php

namespace CodingMs\AddressManager\Controller;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AddressManager\Domain\Model\Address;
use CodingMs\AddressManager\Domain\Model\AddressGroup;
use CodingMs\AddressManager\Domain\Model\AddressOrganisation;
use CodingMs\AddressManager\Domain\Model\AddressPosition;
use CodingMs\AddressManager\Domain\Repository\AddressGroupRepository;
use CodingMs\AddressManager\Domain\Repository\AddressOrganisationRepository;
use CodingMs\AddressManager\Domain\Repository\AddressPositionRepository;
use CodingMs\AddressManager\Domain\Repository\AddressRepository;
use CodingMs\AddressManager\Utility\ToolsUtility;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Result;
use Exception;
use PDO;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\LanguageAspect;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Persistence\Generic\QueryResult;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Fluid\View\StandaloneView;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * JsonApiController
 * @noinspection PhpUnused
 */
class JsonApiController extends ActionController
{
    /**
     * @var array<string, mixed>
     */
    protected array $json = [];

    /**
     * @var int
     */
    protected int $languageUid = 0;

    /**
     * @var AddressRepository
     */
    protected AddressRepository $addressRepository;

    /**
     * @var AddressGroupRepository
     */
    protected AddressGroupRepository $addressGroupRepository;

    /**
     * @var AddressPositionRepository
     */
    protected AddressPositionRepository $addressPositionRepository;

    /**
     * @var AddressOrganisationRepository
     */
    protected AddressOrganisationRepository $addressOrganisationRepository;

    /**
     * @param AddressRepository $addressRepository
     * @param AddressGroupRepository $addressGroupRepository
     * @param AddressPositionRepository $addressPositionRepository
     * @param AddressOrganisationRepository $addressOrganisationRepository
     */
    public function __construct(
        AddressRepository $addressRepository,
        AddressGroupRepository $addressGroupRepository,
        AddressPositionRepository $addressPositionRepository,
        AddressOrganisationRepository $addressOrganisationRepository
    ) {
        $this->addressRepository = $addressRepository;
        $this->addressGroupRepository = $addressGroupRepository;
        $this->addressPositionRepository = $addressPositionRepository;
        $this->addressOrganisationRepository = $addressOrganisationRepository;
    }

    /**
     * @return void
     * @throws \TYPO3\CMS\Core\Context\Exception\AspectNotFoundException
     */
    public function initializeAction(): void
    {
        //
        if (!isset($this->settings['framework']) || trim($this->settings['framework']) === '{$themes.framework}') {
            $this->settings['framework'] = 'Bootstrap4';
        }
        /** @var Context $context */
        $context = GeneralUtility::makeInstance(Context::class);
        /** @var LanguageAspect $languageAspect */
        $languageAspect = $context->getAspect('language');
        $this->languageUid = $languageAspect->getId();
    }

    /**
     * @return ResponseInterface
     * @throws NoSuchArgumentException
     * @throws \JsonException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function selectAction(): ResponseInterface
    {
        $this->settings['pro'] = (bool)ExtensionManagementUtility::isLoaded('address_manager_pro');
        if (!isset($this->settings['list'])) {
            $this->settings['list'] = [];
        }
        if (!empty($this->settings['debug'])) {
            if (!isset($this->settings['list']['debug'])) {
                $this->settings['list']['debug'] = [];
            }
            $this->settings['list']['debug'][] = 'sys_language_uid:' . $this->languageUid;
        }
        $this->settings['request'] = $this->request->getArguments();
        //
        $partialFileName = 'Address/List/Item/' . $this->settings['framework'] . '.html';
        $configurationType = ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK;
        $configuration = $this->configurationManager->getConfiguration($configurationType, 'AddressManager');
        $partialRootPath = ToolsUtility::getTemplatePath($configuration['view'], 'partial', $partialFileName);
        if (!empty($this->settings['debug'])) {
            $this->settings['list']['debug'][] = 'partialFileName: ' . $partialFileName;
            $this->settings['list']['debug'][] = 'partialRootPath: ' . $partialRootPath;
        }
        //
        // Get data/settings from calling plugin
        $this->settings['list']['uid'] = 0;
        if ($this->request->hasArgument('listUid')) {
            $this->settings['list']['uid'] = (int)$this->request->getArgument('listUid');
            if ($this->settings['list']['uid'] > 0) {
                $this->mergeFlexFormSettings();
            }
        }
        //
        // Explode filter fields
        $this->explodeListFilterFields();
        $this->validateListLimit();
        $this->validateListOffset();
        //
        // Available groups/positions/organisations from settings
        if (!empty($this->settings['list']['allowedGroups'])) {
            $this->settings['list']['allowedGroups'] = GeneralUtility::trimExplode(',', $this->settings['list']['allowedGroups']);
        } else {
            $this->settings['list']['allowedGroups'] = [];
        }
        if (!empty($this->settings['list']['allowedPositions'])) {
            $this->settings['list']['allowedPositions'] = GeneralUtility::trimExplode(',', $this->settings['list']['allowedPositions']);
        } else {
            $this->settings['list']['allowedPositions'] = [];
        }
        if (!empty($this->settings['list']['allowedOrganisations'])) {
            $this->settings['list']['allowedOrganisations'] = GeneralUtility::trimExplode(',', $this->settings['list']['allowedOrganisations']);
        } else {
            $this->settings['list']['allowedOrganisations'] = [];
        }
        //
        $order = 0;
        $storagePid = 0;
        $locations = null;
        $selectedGroup = null;
        $selectedPosition = null;
        $selectedOrganisation = null;
        //
        // Filter selects
        $filterSelectFirst = null;
        $filterSelectSecond = null;
        $filterSelectThird = null;
        //
        // Identify filter and their order
        $filter = [];
        foreach ($this->settings['list']['filterFields'] ?? [] as $filterKey) {
            if (lcfirst($filterKey) == 'group') {
                $selectedGroup = $this->validateAddressGroup($filterKey, $order);
                if ($selectedGroup) {
                    $filter[$order] = $selectedGroup;
                    $storagePid = $selectedGroup->getPid();
                }
                $order++;
            } elseif (lcfirst($filterKey) == 'position') {
                $selectedPosition = $this->validateAddressPosition($filterKey, $order);
                if ($selectedPosition) {
                    $filter[$order] = $selectedPosition;
                    $storagePid = $selectedPosition->getPid();
                }
                $order++;
            } elseif (lcfirst($filterKey) == 'organisation') {
                $selectedOrganisation = $this->validateAddressOrganisation($filterKey, $order);
                if ($selectedOrganisation) {
                    $filter[$order] = $selectedOrganisation;
                    $storagePid = $selectedOrganisation->getPid();
                }
                $order++;
            } elseif (lcfirst($filterKey) == 'name') {
                $searchWord = $this->validateAddressSearchWord($filterKey, $order);
                if ($searchWord) {
                    $filter[$order] = $searchWord;
                }
                $order++;
            } elseif (lcfirst($filterKey) == 'customString') {
                $customString = $this->validateAddressCustomString($filterKey, $order);
                if ($customString) {
                    $filter[$order] = [
                        'customString' => [
                            'value' => $customString,
                            'field' => $this->settings['list']['customString']['field'] ?? ''
                        ]
                    ];
                }
                $order++;
            } elseif (lcfirst($filterKey) == 'location' && ExtensionManagementUtility::isLoaded('address_manager_pro')) {
                $locations = $this->validateLocation();
                if (is_array($locations)) {
                    if (!empty($this->settings['debug'])) {
                        $this->settings['list']['debug'][] = count($locations) . ' locations found!';
                    }
                    $filter[$order] = $locations;
                } elseif ($locations === null && $this->settings['debug']) {
                    $this->settings['list']['debug'][] = 'locations is null! That means there were no valid geo coordinates found.';
                }
                $order++;
            }
        }
        //
        // Get delimited select items for second filter
        if (!empty($this->settings['list']['filterFieldsByOrder'])) {
            //
            // Get all assigned address.uids for the first select box selection
            $selectFirstUid = (int)$this->settings['list']['filterFieldsByOrder'][0]['uid'] ?? 0;
            $selectFirstType = $this->settings['list']['filterFieldsByOrder'][0]['type'] ?? '';
            $addressUids = $this->getAddressUidsByRelation($selectFirstType, $selectFirstUid);
            if (!empty($this->settings['debug'])) {
                $this->settings['list']['debug'][] = count($addressUids) . ' address uids found';
            }
            //
            // Get delimited select items for second filter
            if (count($this->settings['list']['filterFieldsByOrder']) >= 2) {
                $selectSecondUid = $this->settings['list']['filterFieldsByOrder'][1]['uid'] ?? 0;
                $selectSecondType = $this->settings['list']['filterFieldsByOrder'][1]['type'] ?? '';
                $uidsFormThirdStep = [];
                if (count($addressUids) > 0) {
                    //
                    // Select for this addresses the select box entries for the next filter
                    $relations = $this->getRelationsForSelection($selectSecondType, $addressUids);
                    if (count($relations) > 0) {
                        foreach ($relations as $item) {
                            $itemArray = [];
                            $itemArray['uid'] = $item['uid'] ?? 0;
                            $itemArray['title'] = $item['title'] ?? '';
                            if ($this->languageUid > 0) {
                                $title = $this->getRelationTranslationForSelection($selectSecondType, $item['uid'] ?? 0);
                                if (trim($title) !== '') {
                                    $itemArray['title'] = $title;
                                }
                            }
                            if (!isset($this->settings['list']['filterFieldSecond'])) {
                                $this->settings['list']['filterFieldSecond'] = [];
                            }
                            if (!isset($this->settings['list']['filterFieldSecond']['items'])) {
                                $this->settings['list']['filterFieldSecond']['items'] = [];
                            }
                            if (!empty($itemArray['title']) && !isset($this->settings['list']['filterFieldSecond']['items'][$itemArray['title']])) {
                                $this->settings['list']['filterFieldSecond']['items'][$itemArray['title']] = $itemArray;
                            }
                            // Collect address uid
                            if (!empty($item['uid_local'])) {
                                $uidsFormThirdStep[$item['uid_local']] = $item['uid_local'];
                            }
                        }
                    }
                }
                $this->settings['list']['filterFieldSecond']['selected'] = $selectSecondUid;
                //
                // Get delimited select items for third filter
                if (count($this->settings['list']['filterFieldsByOrder']) >= 3 && $selectSecondUid > 0) {
                    if (!empty($this->settings['list']['filterFieldSecond']['items'])) {
                        //
                        // Get all assigned address.uids for the second select box selection
                        $addressUids = $this->getAddressUidsByRelation($selectSecondType, $selectSecondUid, $uidsFormThirdStep);
                        if (!empty($this->settings['debug'])) {
                            $this->settings['list']['debug'][] = count($addressUids) . ' address uids found';
                        }
                        if (count($addressUids) > 0) {
                            if (count($this->settings['list']['filterFieldsByOrder']) > 2) {
                                $selectThirdUid = $this->settings['list']['filterFieldsByOrder'][2]['uid'] ?? 0;
                                $selectThirdType = $this->settings['list']['filterFieldsByOrder'][2]['type'] ?? '';
                                //
                                // Select for this addresses the select box entries for the next filter
                                $relations = $this->getRelationsForSelection($selectThirdType, $addressUids);
                                if (count($relations) > 0) {
                                    foreach ($relations as $item) {
                                        $itemArray = [];
                                        $itemArray['uid'] = $item['uid'] ?? 0;
                                        $itemArray['title'] = $item['title'] ?? '';
                                        if ($this->languageUid > 0) {
                                            $title = $this->getRelationTranslationForSelection($selectThirdType, $item['uid'] ?? 0);
                                            if (trim($title) !== '') {
                                                $itemArray['title'] = $title;
                                            }
                                        }
                                        if (!isset($this->settings['list']['filterFieldThird'])) {
                                            $this->settings['list']['filterFieldThird'] = [];
                                        }
                                        if (!isset($this->settings['list']['filterFieldThird']['items'])) {
                                            $this->settings['list']['filterFieldThird']['items'] = [];
                                        }
                                        if (!isset($this->settings['list']['filterFieldThird']['items'][$itemArray['title']])) {
                                            $this->settings['list']['filterFieldThird']['items'][$itemArray['title']] = $itemArray;
                                        }
                                    }
                                }
                                $this->settings['list']['filterFieldThird']['selected'] = $selectThirdUid;
                            }
                        }
                    }
                }
            }
        }
        //
        // Render list items
        $this->settings['list']['items'] = [];
        $this->settings['list']['count'] = 0;
        //
        // $storagePid is filled by selected group/position/organisation
        // -> set a fallback to:
        // FlexForm alternativeStoragePid
        // or
        // persistence.storagePid from TypoScript!
        if ($storagePid === 0 && !empty($this->settings['alternativeStoragePid'])) {
            $storagePid = (int)$this->settings['alternativeStoragePid'];
        } elseif ($storagePid === 0) {
            $storagePid = (int)($configuration['persistence']['storagePid'] ?? 0);
        }
        //
        // Only search for filtered addresses, if the location filter has returned any address.
        // Otherwise there can't be more address results!
        //if($locations !== null && count($locations) > 0) {
        $this->settings['list']['count'] = $this->addressRepository->countAllByFilter($filter);
        /** @var QueryResult $addressItems */
        $addressItems = $this->addressRepository->findAllByFilter(
            $filter,
            $this->settings['list']['sortBy'] ?? 'lastnameThenFirstname',
            'ASC',
            $this->settings['list']['offset'] ?? 0,
            $this->settings['list']['limit'] ?? 0,
            $storagePid
        );
        if (!empty($this->settings['debug'])) {
            $this->settings['list']['countBeforeAllowedCondition'] = count($addressItems);
            $this->settings['list']['mustLocatedOnPageUid'] = (int)($configuration['persistence']['storagePid'] ?? 0);
            $this->settings['list']['mustLocatedOnStoragePid'] = $storagePid;
        }
        if ($addressItems->count() > 0) {
            $itemCount = 0;
            /** @var Address $addressItem */
            foreach ($addressItems as $addressItem) {
                // Workaround: Ensure to display only items from the selected storage.
                // @todo This should be solved by database queries!
                if (!empty($storagePid) && $addressItem->getPid() != $storagePid) {
                    continue;
                }

                if ($this->isAllowedAddress($addressItem) && !$this->isLimitReached($itemCount)) {
                    $uid = $addressItem->getUid();
                    $addressArray = $addressItem->toArray(true);
                    /** @var StandaloneView $listItemView */
                    $listItemView = GeneralUtility::makeInstance(StandaloneView::class);
                    if((int)VersionNumberUtility::getCurrentTypo3Version() >= 12) {
                        $listItemView->setRequest($this->request);
                    }
                    $listItemView->setTemplatePathAndFilename($partialRootPath . $partialFileName);
                    $listItemView->assign('settings', $this->settings);
                    $listItemView->assign('address', $addressItem);
                    $listItemView->assign('useWrapper', 1);
                    $addressArray['uid'] = $uid;
                    $addressArray['distance'] = $addressItem->getDistance();
                    $addressArray['distanceOrder'] = (float)(((string)$addressItem->getDistance()) . ((string)$itemCount));
                    $addressArray['distanceOrder'] = (int)(($addressArray['distanceOrder'] ?? 0) * 100000000000000);
                    $addressArray['html'] = base64_encode($listItemView->render());
                    //
                    // Sort order for result items
                    // Default order is by distance
                    if (isset($this->settings['list']['orderForAjaxResult']) && $this->settings['list']['orderForAjaxResult'] === 'distance') {
                        $this->settings['list']['items'][$addressArray['distanceOrder']] = $addressArray;
                    } else {
                        // Order by name
                        $this->settings['list']['items'][] = $addressArray;
                    }
                }
                $itemCount++;
            }
            // Convert array keys back to usual, ascendancy integers.
            ksort($this->settings['list']['items']);
            $this->settings['list']['items'] = array_values($this->settings['list']['items']);
        }
        //}
        // Unset unnecessary sub arrays
        if (empty($this->settings['debug'])) {
            unset($this->settings['request']);
            unset($this->settings['detail']);
            unset($this->settings['list']['debug']);
        }
        $response = $this->responseFactory->createResponse()
            ->withHeader('Content-Type', 'application/json; charset=utf-8');
        $response->getBody()->write(json_encode($this->settings, JSON_THROW_ON_ERROR));

        return $response;
    }

    /**
     * @param int $itemCount
     * @return bool
     */
    protected function isLimitReached(int $itemCount): bool
    {
        $limit = (int)($this->settings['list']['limit'] ?? 0);
        $limitReached = false;
        if ($limit > 0 && $itemCount >= $limit) {
            $limitReached = true;
        }
        return $limitReached;
    }

    /**
     * @param Address $addressItem
     * @return bool
     */
    protected function isAllowedAddress(Address$addressItem): bool
    {
        if (!empty($this->settings['list']['allowedGroups']) && is_array($this->settings['list']['allowedGroups'])) {
            $groups = $addressItem->getGroups();
            foreach ($groups as $group) {
                if (in_array($group->getUid(), $this->settings['list']['allowedGroups'])) {
                    return true;
                }
            }
        }
        if (!empty($this->settings['list']['allowedPositions']) && is_array($this->settings['list']['allowedPositions'])) {
            $positions = $addressItem->getPositions();
            foreach ($positions as $position) {
                if (in_array($position->getUid(), $this->settings['list']['allowedPositions'])) {
                    return true;
                }
            }
        }
        if (!empty($this->settings['list']['allowedOrganisations']) && is_array($this->settings['list']['allowedOrganisations'])) {
            $organisations = $addressItem->getOrganisations();
            foreach ($organisations as $organisation) {
                if (in_array($organisation->getUid(), $this->settings['list']['allowedOrganisations'])) {
                    return true;
                }
            }
        }
        if (empty($this->settings['list']['allowedGroups']) && empty($this->settings['list']['allowedPositions']) && empty($this->settings['list']['allowedOrganisations'])) {
            return true;
        }
        return false;
    }

    /**
     * @param string$filterKey
     * @param int$order
     * @return AddressGroup|null
     * @throws NoSuchArgumentException
     */
    protected function validateAddressGroup(string $filterKey, int $order): ?AddressGroup
    {
        $selectedGroup = null;
        $filterField = [];
        if ($this->request->hasArgument('group')) {
            $filterField['uid'] = (int)$this->request->getArgument('group');
            $filterField['type'] = 'group';
            $filterField['order'] = $order;
            // uid must be greater zero
            if ($filterField['uid'] > 0) {
                // uid must be available in storage
                /** @var AddressGroup $selectedGroup */
                $selectedGroup = $this->addressGroupRepository->findByIdentifier($filterField['uid']);
                if ($selectedGroup instanceof AddressGroup) {
                    if (empty($this->settings['list']['allowedGroups']) || in_array($filterField['uid'], $this->settings['list']['allowedGroups'])) {
                        $filterField = array_merge($selectedGroup->toArray(true), $filterField);
                    }
                }
            }
            unset($filterField['description']);
        }
        if (!isset($this->settings['list']['filterFields'])) {
            $this->settings['list']['filterFields'] = [];
        }
        if (!isset($this->settings['list']['filterFieldsByOrder'])) {
            $this->settings['list']['filterFieldsByOrder'] = [];
        }
        $this->settings['list']['filterFields'][$filterKey] = $filterField;
        $this->settings['list']['filterFieldsByOrder'][$order] = $filterField;
        return $selectedGroup;
    }

    /**
     * @param string $filterKey
     * @param int $order
     * @return AddressPosition|null
     */
    protected function validateAddressPosition(string$filterKey, int$order): ?AddressPosition
    {
        $selectedPosition = null;
        $filterField = [];
        if ($this->request->hasArgument('position')) {
            $filterField['uid'] = (int)$this->request->getArgument('position');
            $filterField['type'] = 'position';
            $filterField['order'] = $order;
            // uid must be greater zero
            if ($filterField['uid'] > 0) {
                // uid must be available in storage
                /** @var AddressPosition $selectedPosition */
                $selectedPosition = $this->addressPositionRepository->findByIdentifier($filterField['uid']);
                if ($selectedPosition instanceof AddressPosition) {
                    // uid must be available in plugin settings
                    if (empty($this->settings['list']['allowedPositions']) || in_array($filterField['uid'], $this->settings['list']['allowedPositions'])) {
                        $filterField = array_merge($selectedPosition->toArray(true), $filterField);
                    }
                }
            }
        }
        if (!isset($this->settings['list']['filterFields'])) {
            $this->settings['list']['filterFields'] = [];
        }
        if (!isset($this->settings['list']['filterFieldsByOrder'])) {
            $this->settings['list']['filterFieldsByOrder'] = [];
        }
        $this->settings['list']['filterFields'][$filterKey] = $filterField;
        $this->settings['list']['filterFieldsByOrder'][$order] = $filterField;
        return $selectedPosition;
    }

    /**
     * @param string $filterKey
     * @param int $order
     * @return AddressOrganisation|null
     */
    protected function validateAddressOrganisation(string$filterKey, int$order): ?AddressOrganisation
    {
        $selectedOrganisation = null;
        $filterField = [];
        if ($this->request->hasArgument('organisation')) {
            $filterField['uid'] = (int)$this->request->getArgument('organisation');
            $filterField['type'] = 'organisation';
            $filterField['order'] = $order;
            // uid must be greater zero
            if ($filterField['uid'] > 0) {
                // uid must be available in storage
                /** @var AddressOrganisation $selectedOrganisation */
                $selectedOrganisation = $this->addressOrganisationRepository->findByIdentifier($filterField['uid']);
                if ($selectedOrganisation instanceof AddressOrganisation) {
                    // uid must be available in plugin settings
                    if (empty($this->settings['list']['allowedOrganisations']) || in_array($filterField['uid'], $this->settings['list']['allowedOrganisations'])) {
                        $filterField = array_merge($selectedOrganisation->toArray(true), $filterField);
                    }
                }
            }
        }
        if (!isset($this->settings['list']['filterFields'])) {
            $this->settings['list']['filterFields'] = [];
        }
        if (!isset($this->settings['list']['filterFieldsByOrder'])) {
            $this->settings['list']['filterFieldsByOrder'] = [];
        }
        $this->settings['list']['filterFields'][$filterKey] = $filterField;
        $this->settings['list']['filterFieldsByOrder'][$order] = $filterField;
        return $selectedOrganisation;
    }

    /**
     * @param string $filterKey
     * @param int $order
     * @return string
     */
    protected function validateAddressSearchWord(string$filterKey, int$order): string
    {
        $searchWord = '';
        $filterField = [];
        if ($this->request->hasArgument('searchWord')) {
            $searchWord = $this->request->getArgument('searchWord');
            if (trim($searchWord) != '') {
                $filterField['value'] = $searchWord;
            }
            $filterField['order'] = $order;
        }
        if (!isset($this->settings['list']['filterFields'])) {
            $this->settings['list']['filterFields'] = [];
        }
        $this->settings['list']['filterFields'][$filterKey] = $filterField;
        return $searchWord;
    }

    /**
     * @param string $filterKey
     * @param int $order
     * @return string
     */
    protected function validateAddressCustomString(string$filterKey, int$order): string
    {
        $customString = '';
        $filterField = [];
        if ($this->request->hasArgument('customString')) {
            $customString = $this->request->getArgument('customString');
            if (trim($customString) != '') {
                $filterField['value'] = $customString;
            }
            $filterField['order'] = $order;
        }
        if (!isset($this->settings['list']['filterFields'])) {
            $this->settings['list']['filterFields'] = [];
        }
        $this->settings['list']['filterFields'][$filterKey] = $filterField;
        return $customString;
    }

    /**
     * @return void
     */
    protected function validateListLimit()
    {
        if ($this->request->hasArgument('limit')) {
            $this->settings['list']['limit'] = (int)$this->request->getArgument('limit');
        } else {
            $this->settings['list']['limit'] = (int)($this->settings['list']['limit'] ?? 0);
        }
    }

    /**
     * @return void
     */
    protected function validateListOffset()
    {
        if ($this->request->hasArgument('offset')) {
            $this->settings['list']['offset'] = (int)$this->request->getArgument('offset');
        } else {
            $this->settings['list']['offset'] = (int)($this->settings['list']['offset'] ?? 0);
        }
    }

    /**
     * Merge flex form settings with TypoScript setup settings
     *
     * @return void
     */
    protected function mergeFlexFormSettings()
    {
        $flexFormData = [];
        $resultArray = BackendUtility::getRecord('tt_content', (int)($this->settings['list']['uid'] ?? 0));
        if ($resultArray) {
            if((int)VersionNumberUtility::getCurrentTypo3Version() > 11) {
                /** @var ContentObjectRenderer $contentObject */
                $contentObject = $this->request->getAttribute('currentContentObject');
            } else {
                /** @var ContentObjectRenderer $contentObject */
                $contentObject = $this->configurationManager->getContentObject();
            }
            $contentObject->readFlexformIntoConf($resultArray['pi_flexform'], $flexFormData);
            if (!empty($flexFormData)) {
                foreach ($flexFormData as $key => $value) {
                    if (substr($key, 0, 9) == 'settings.') {
                        $keyParts = explode('.', substr($key, 9));
                        if (count($keyParts) == 1) {
                            $this->settings[$keyParts[0]] = $value;
                        } elseif (count($keyParts) == 2) {
                            if (!isset($this->settings[$keyParts[0]])) {
                                $this->settings[$keyParts[0]] = [];
                            }
                            $this->settings[$keyParts[0]][$keyParts[1]] = $value;
                        }
                    }
                }
            }
        }
    }

    /**
     * If valid location found, it will return an array with 0 or more found locations.
     * If the location is invalid or zero, it will return null.
     *
     * @return array<mixed>|QueryResultInterface|null
     * @throws NoSuchArgumentException
     */
    protected function validateLocation()
    {
        $location = [];
        $locations = null;
        if ($this->request->hasArgument('latitude')) {
            $location['latitude'] = (float)$this->request->getArgument('latitude');
        }
        if ($this->request->hasArgument('latitude')) {
            $location['longitude'] = (float)$this->request->getArgument('longitude');
        }
        if ($this->request->hasArgument('distance')) {
            $location['distance'] = (int)$this->request->getArgument('distance');
        }
        if (isset($location['latitude']) && $location['latitude'] !== 0.0 && isset($location['longitude']) && $location['longitude'] !== 0.0) {
            //
            // In the next step, we're converting the geo coordinates into address uid in location distance!
            $this->settings['list']['location'] = $location;
            $locations = $this->addressRepository->findByLocation($location);
            if (!empty($this->settings['debug'] && isset($location['distance']))) {
                $this->settings['list']['debug'][] = 'findByLocation(' . $location['latitude'] . ', ' . $location['longitude'] . ', ' . $location['distance'] . ')';
                $this->settings['list']['debug'][] = count($locations) . ' addresses/locations found';
            }
        }
        return $locations;
    }

    /**
     * Explodes all list filter fields
     *
     * @return void
     */
    protected function explodeListFilterFields(): void
    {
        if (empty($this->settings['list']['filterFields'])) {
            $this->settings['list']['filterFields'] = [];
        } else {
            $this->settings['list']['filterFields'] = explode(',', $this->settings['list']['filterFields']);
            $this->settings['list']['filterFields'] = array_combine($this->settings['list']['filterFields'], $this->settings['list']['filterFields']);
        }
    }

    /**
     * Returns all address uids, which have a relation that met the type (group, position, organisation)
     *
     * @param string $type
     * @param int $uid
     * @param array<int|string, mixed> $addressUids
     * @return array<int|string, mixed>
     */
    protected function getAddressUidsByRelation(string $type, int $uid, array $addressUids = []): array
    {
        if (!empty($this->settings['debug'])) {
            $this->settings['list']['debug'][] = 'getAddressUidsByRelation(\'' . $type . '\', ' . $uid . ', \'' . implode(', ', $addressUids) . '\')';
        }
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_addressmanager_domain_model_address');
        $whereAnd = [];
        $whereAnd[] = 'tx_addressmanager_address_address' . $type . '_mm.uid_foreign=' . (int)$uid;
        $whereAnd[] = 'tx_addressmanager_address_address' . $type . '_mm.uid_local=tx_addressmanager_domain_model_address.uid';
        //
        // Only matched addresses uids from earlier select boxes
        if (count($addressUids) > 0) {
            $whereAnd[] = 'tx_addressmanager_address_address' . $type . '_mm.uid_local IN (' . implode(',', $addressUids) . ')';
        }
        $queryBuilder
            ->select('uid')
            ->from('tx_addressmanager_domain_model_address')
            ->join(
                'tx_addressmanager_domain_model_address',
                'tx_addressmanager_address_address' . $type . '_mm',
                'tx_addressmanager_address_address' . $type . '_mm',
                implode(' AND ', $whereAnd)
            );
        if (!empty($this->settings['debug'])) {
            if (!isset($this->settings['list']['debug'])) {
                $this->settings['list']['debug'] = [];
            }
            $this->settings['list']['debug'][] = $queryBuilder->getSQL();
        }
        $addressUids = [];
        /** @var Result $result */
        $result = $queryBuilder->execute();
        while ($row = $result->fetch()) {
            $addressUids[] = (int)$row['uid'];
        }
        return $addressUids;
    }

    /**
     * Get select box values (relations of addresses) based on available addresses
     *
     * @param string $type
     * @param array<int|string, mixed> $addressUids
     * @return array<mixed>
     */
    protected function getRelationsForSelection(string $type, array $addressUids): array
    {
        if (!empty($this->settings['debug'])) {
            if (!isset($this->settings['list']['debug'])) {
                $this->settings['list']['debug'] = [];
            }
            $this->settings['list']['debug'][] = 'getRelationsForSelection(\'' . $type . '\', ' . implode(', ', $addressUids) . ')';
        }
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_addressmanager_domain_model_address' . $type);
        $whereAnd = [];
        $whereAnd[] = 'tx_addressmanager_address_address' . $type . '_mm.uid_local IN (' . implode(', ', $addressUids) . ')';
        $whereAnd[] = 'tx_addressmanager_address_address' . $type . '_mm.uid_foreign = tx_addressmanager_domain_model_address' . $type . '.uid';
        $whereAnd[] = 'tx_addressmanager_domain_model_address' . $type . '.sys_language_uid IN (0,-1)';
        $queryBuilder
            ->select(
                'tx_addressmanager_address_address' . $type . '_mm.uid_foreign AS uid',
                'tx_addressmanager_address_address' . $type . '_mm.uid_local AS uid_local',
                'tx_addressmanager_domain_model_address' . $type . '.title AS title',
                'tx_addressmanager_domain_model_address' . $type . '.l10n_parent AS l10n_parent'
            )
            ->from('tx_addressmanager_domain_model_address' . $type)
            ->join(
                'tx_addressmanager_domain_model_address' . $type,
                'tx_addressmanager_address_address' . $type . '_mm',
                'tx_addressmanager_address_address' . $type . '_mm',
                implode(' AND ', $whereAnd)
            )
            ->orderBy('tx_addressmanager_domain_model_address' . $type . '.sorting'); //->groupBy('uid');
        if (!empty($this->settings['debug'])) {
            $this->settings['list']['debug'][] = $queryBuilder->getSQL();
        }
        /** @var Result $result */
        $result = $queryBuilder->execute();
        return $result->fetchAll();
    }

    /**
     * Get select box values (relations of addresses) based on available addresses
     *
     * @param string $type
     * @param int $uid
     * @return string
     */
    protected function getRelationTranslationForSelection(string $type, int $uid): string
    {
        if (!empty($this->settings['debug'])) {
            if (!isset($this->settings['list']['debug'])) {
                $this->settings['list']['debug'] = [];
            }
            $this->settings['list']['debug'][] = 'getRelationTranslationForSelection(\'' . $type . '\', ' . $uid . ')';
        }
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_addressmanager_domain_model_address' . $type);
        $queryBuilder
            ->select('title')
            ->from('tx_addressmanager_domain_model_address' . $type)
            ->where('sys_language_uid = ' . $this->languageUid . ' AND l10n_parent=' . (int)$uid);
        if (!empty($this->settings['debug'])) {
            $this->settings['list']['debug'][] = $queryBuilder->getSQL();
        }
        /** @var Result $resource */
        $resource = $queryBuilder->execute();
        $row = $resource->fetch(PDO::FETCH_ASSOC);
        $title = '';
        if (is_array($row) && isset($row['title'])) {
            $title = $row['title'];
            $this->settings['list']['debug'][] = 'Translated title for ' . $type . ':' . $uid . ' -> ' . $title;
        }
        return $title;
    }
}
