<?php

namespace CodingMs\AddressManager\Controller;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AddressManager\Domain\Model\Address;
use CodingMs\AddressManager\Domain\Model\FileReference;
use CodingMs\AddressManager\Domain\Repository\AddressGroupRepository;
use CodingMs\AddressManager\Domain\Repository\AddressOrganisationRepository;
use CodingMs\AddressManager\Domain\Repository\AddressPositionRepository;
use CodingMs\AddressManager\Domain\Repository\AddressRepository;
use CodingMs\AddressManager\PageTitle\PageTitleProvider;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\UserAspect;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerInterface;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerRegistry;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\Generic\QueryResult;
use TYPO3\CMS\Extbase\Persistence\Generic\QuerySettingsInterface;
use TYPO3\CMS\Extbase\Service\ImageService;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * AddressController
 */
class AddressController extends ActionController
{
    /**
     * @var AddressRepository
     */
    protected AddressRepository $addressRepository;

    /**
     * @var AddressGroupRepository
     */
    protected AddressGroupRepository $addressGroupRepository;

    /**
     * @var AddressPositionRepository
     */
    protected AddressPositionRepository $addressPositionRepository;

    /**
     * @var AddressOrganisationRepository
     */
    protected AddressOrganisationRepository $addressOrganisationRepository;

    /**
     * @param AddressRepository $addressRepository
     * @param AddressGroupRepository $addressGroupRepository
     * @param AddressPositionRepository $addressPositionRepository
     * @param AddressOrganisationRepository $addressOrganisationRepository
     */
    public function __construct(
        AddressRepository $addressRepository,
        AddressGroupRepository $addressGroupRepository,
        AddressPositionRepository $addressPositionRepository,
        AddressOrganisationRepository $addressOrganisationRepository
    ) {
        $this->addressRepository = $addressRepository;
        $this->addressGroupRepository = $addressGroupRepository;
        $this->addressPositionRepository = $addressPositionRepository;
        $this->addressOrganisationRepository = $addressOrganisationRepository;
    }

    /**
     * Initialize actions
     */
    public function initializeAction(): void
    {
        //
        if (!isset($this->settings['framework']) || trim($this->settings['framework'] ?? '') === '{$themes.framework}') {
            $this->settings['framework'] = 'Bootstrap4';
        }
        //
        // Use an alternative record storage?
        if (!empty($this->settings['alternativeStoragePid'])) {
            /** @var QuerySettingsInterface $defaultQuerySettings */
            $defaultQuerySettings = GeneralUtility::makeInstance(QuerySettingsInterface::class);
            $defaultQuerySettings->setStoragePageIds([(int)$this->settings['alternativeStoragePid']]);
            $this->addressRepository->setDefaultQuerySettings($defaultQuerySettings);
            $this->addressGroupRepository->setDefaultQuerySettings($defaultQuerySettings);
            $this->addressPositionRepository->setDefaultQuerySettings($defaultQuerySettings);
            $this->addressOrganisationRepository->setDefaultQuerySettings($defaultQuerySettings);
        }
        //
    }

    /**
     * @return ResponseInterface
     * @throws InvalidQueryException
     */
    public function listAction(): ResponseInterface
    {
        // Static template available?
        // Debug extension?
        if (!isset($this->settings['debug'])) {
            $messageBody = 'Please include the static template!';
            $messageTitle = 'Error';
            $this->addFlashMessage($messageBody, $messageTitle, AbstractMessage::ERROR);
            return $this->htmlResponse();;
        }
        if (empty($this->settings['list']['defaultPid'])) {
            $messageBody = 'Please set the page uid of the default address list page!';
            $messageTitle = 'Error';
            $this->addFlashMessage($messageBody, $messageTitle, AbstractMessage::ERROR);
            return $this->htmlResponse();;
        }
        $this->settings['debug'] = (boolean)$this->settings['debug'];
        //
        // Display type
        if (empty($this->settings['displayType'])) {
            $this->settings['displayType'] = 'Default';
        }
        //
        // Get offset/limit from configuration
        $limit = (int)($this->settings['list']['limit'] ?? 0);
        $offset = (int)($this->settings['list']['offset'] ?? 0);
        // Overwrite limit with user setting
        if ($this->request->hasArgument('limit')) {
            $limit = (int)$this->request->getArgument('limit');
        }
        // Ensure the limit isn't empty
        if ($limit == 0) {
            $limit = 9999;
        }
        //
        // Get sort by
        $sortBy = $this->settings['list']['sortBy'] ?? '';
        // Get allowed groups for filter function
        $allowedGroups = [];
        if (!empty($this->settings['list']['allowedGroups'])) {
            $allowedGroups = explode(',', $this->settings['list']['allowedGroups']);
        }
        //
        // Get allowed positions for filter function
        $allowedPositions = [];
        if (!empty($this->settings['list']['allowedPositions'])) {
            $allowedPositions = explode(',', $this->settings['list']['allowedPositions']);
        }
        //
        // Get allowed organisations for filter function
        $allowedOrganisations = [];
        if (!empty($this->settings['list']['allowedOrganisations'])) {
            $allowedOrganisations = explode(',', $this->settings['list']['allowedOrganisations']);
        }
        //
        // Identify the current selected group
        $group = null;
        if ($this->request->hasArgument('groupSelect')) {
            $groupUid = (int)$this->request->getArgument('groupSelect');
            $group = $this->addressGroupRepository->findByIdentifier($groupUid);
        }
        // Identify the current selected position
        $position = null;
        if ($this->request->hasArgument('positionSelect')) {
            $positionUid = (int)$this->request->getArgument('positionSelect');
            $position = $this->addressPositionRepository->findByIdentifier($positionUid);
        }
        // Identify the current selected organisation
        $organisation = null;
        if ($this->request->hasArgument('organisationSelect')) {
            $organisationUid = (int)$this->request->getArgument('organisationSelect');
            $organisation = $this->addressOrganisationRepository->findByIdentifier($organisationUid);
        }
        //
        /** @var QueryResult $groupSelect */
        $groupSelect = $this->addressGroupRepository->findAllByAllowedGroups(
            $allowedGroups,
            $this->settings['list']['group']['sortBy'] ?? '',
            $this->settings['list']['group']['sortOrder'] ?? ''
        );
        /** @var QueryResult $positionSelect */
        $positionSelect = $this->addressPositionRepository->findAllByAllowedPositions(
            $allowedPositions,
            $this->settings['list']['position']['sortBy'] ?? '',
            $this->settings['list']['position']['sortOrder'] ?? ''
        );
        /** @var QueryResult $organisationSelect */
        $organisationSelect = $this->addressOrganisationRepository->findAllByAllowedOrganisations(
            $allowedOrganisations,
            $this->settings['list']['organisation']['sortBy'] ?? '',
            $this->settings['list']['organisation']['sortOrder'] ?? ''
        );
        //
        // Get plugins record uid
        if((int)VersionNumberUtility::getCurrentTypo3Version() > 11) {
            /** @var ContentObjectRenderer $contentObject */
            $contentObject = $this->request->getAttribute('currentContentObject');
        } else {
            /** @var ContentObjectRenderer $contentObject */
            $contentObject = $this->configurationManager->getContentObject();
        }
        $content = $contentObject->data;
        /** @var int $addressCount */
        $addressCount = $this->addressRepository->countAllForInit(
            $groupSelect,
            $positionSelect,
            $organisationSelect
        );
        /** @var QueryResult $address */
        $address = $this->addressRepository->findAllForInit(
            $groupSelect,
            $positionSelect,
            $organisationSelect,
            $sortBy,
            'ASC',
            $offset,
            $limit
        );
        if ($address->count() === 0) {
            $messageKey = 'tx_addressmanager_message.info_no_addresses_found';
            $message = LocalizationUtility::translate($messageKey, 'AddressManager');
            $this->addFlashMessage((string)$message, '', FlashMessage::INFO);
        }
        // Explode filter fields
        $this->explodeListFilterFields();
        //
        // Location and co.
        $latitude = '';
        if ($this->request->hasArgument('latitude')) {
            $latitude = $this->request->getArgument('latitude');
        }
        $longitude = '';
        if ($this->request->hasArgument('longitude')) {
            $longitude = $this->request->getArgument('longitude');
        }
        $location = '';
        if ($this->request->hasArgument('location')) {
            $location = $this->request->getArgument('location');
        }
        $distance = '';
        if ($this->request->hasArgument('distance')) {
            $distance = $this->request->getArgument('distance');
        }
        $search = '';
        if ($this->request->hasArgument('searchField')) {
            $search = $this->request->getArgument('searchField');
        }
        $customString = '';
        if ($this->request->hasArgument('customStringField')) {
            $customString = $this->request->getArgument('customStringField');
        }
        $this->view->assign('latitude', $latitude);
        $this->view->assign('longitude', $longitude);
        $this->view->assign('location', $location);
        $this->view->assign('distance', $distance);
        $this->view->assign('search', $search);
        $this->view->assign('customString', $customString);
        //
        $this->view->assign('limit', $limit);
        $this->view->assign('settings', $this->settings);
        $this->view->assign('group', $group);
        $this->view->assign('position', $position);
        $this->view->assign('organisation', $organisation);
        // Select box items
        $this->view->assign('groupSelect', $groupSelect);
        $this->view->assign('positionSelect', $positionSelect);
        $this->view->assign('organisationSelect', $organisationSelect);
        // Address items
        $this->view->assign('addressItems', $address);
        $this->view->assign('addressCount', $addressCount);
        // Uid of this plugin
        $this->view->assign('uid', $content['uid']);
        $this->view->assign('content', $content);

        return $this->htmlResponse();
    }

    /**
     * @return ResponseInterface
     * @throws \TYPO3\CMS\Core\Context\Exception\AspectNotFoundException
     * @throws \TYPO3\CMS\Core\Context\Exception\AspectPropertyNotFoundException
     */
    public function showAction(): ResponseInterface
    {
        $address = null;
        $addressUid = 0;
        if ($this->request->hasArgument('address')) {
            $addressUid = (int)$this->request->getArgument('address');
            if ($addressUid > 0) {
                $address = $this->addressRepository->findByIdentifier($addressUid);
            }
        }
        if (!($address instanceof Address)) {
            //
            // If object not found, check if it's a preview object by a logged-in user!
            /** @var Context $context */
            $context = GeneralUtility::makeInstance(Context::class);
            if ($context->getPropertyFromAspect('frontend.user', 'isLoggedIn')) {
                /** @var UserAspect $frontendUserAspect */
                $frontendUserAspect = $context->getAspect('frontend.user');
                $address = $this->addressRepository->findByIdentifierFrontend($addressUid, $frontendUserAspect->get('id'));
                if ($address instanceof Address) {
                    $this->addFlashMessage(
                        (string)LocalizationUtility::translate('tx_addressmanager_message.warning_address_object_is_deactivated', 'AddressManager'),
                        (string)LocalizationUtility::translate('tx_addressmanager_message.warning_headline', 'AddressManager'),
                        AbstractMessage::WARNING
                    );
                }
            }
        }
        if ($address instanceof Address) {
            $this->getMetaInformation($address, $this->settings['siteName'] ?? '');
            $this->view->assign('address', $address);
        } else {
            $this->addFlashMessage(
                (string)LocalizationUtility::translate('tx_addressmanager_message.error_address_object_not_found', 'AddressManager'),
                (string)LocalizationUtility::translate('tx_addressmanager_message.error_headline', 'AddressManager'),
                AbstractMessage::ERROR
            );
            return $this->redirect('list');
        }

        return $this->htmlResponse();
    }

    /**
     * @return ResponseInterface
     */
    public function showSingleAction(): ResponseInterface
    {
        if (!empty($this->settings['address'])) {
            $addressUid = (int)$this->settings['address'];
                            /** @var Address $address */
            $address = $this->addressRepository->findByUid($addressUid);
            $this->getMetaInformation($address, $this->settings['siteName'] ?? '');
            $this->view->assign('address', $address);
        }

        return $this->htmlResponse();
    }

    /**
     * @return ResponseInterface
     */
    public function listSelectedAction(): ResponseInterface
    {
        $addresses = [];
        if (!empty($this->settings['addresses'])) {
            $addresses = GeneralUtility::intExplode(',', $this->settings['addresses'], true);
        }
        $this->view->assign('addressItems', $this->addressRepository->findByUids($addresses));

        return $this->htmlResponse();
    }

    /**
     * @param Address $address
     * @param string $siteName
     * @return void
     */
    protected function getMetaInformation(Address $address, string $siteName = ''): void
    {
        //
        // Get fallback values
        $htmlTitle = trim((string)$address->getHtmlTitle());
        if ($htmlTitle === '') {
            $htmlTitle = $address->getName();
        }
        $description = trim((string)$address->getMetaDescription());
        if ($description === '') {
            $description = substr(strip_tags($address->getDescription()), 0, 160);
        }
        //
        /** @var MetaTagManagerRegistry $metaTagManagerRegistry */
        $metaTagManagerRegistry = GeneralUtility::makeInstance(MetaTagManagerRegistry::class);
        //
        /** @var MetaTagManagerInterface $metaTagManagerDescription */
        $metaTagManagerDescription = $metaTagManagerRegistry->getManagerForProperty('description');
        $metaTagManagerDescription->addProperty('description', $description);
        //
        /** @var MetaTagManagerInterface $metaTagManagerAbstract */
        $metaTagManagerAbstract = $metaTagManagerRegistry->getManagerForProperty('abstract');
        $metaTagManagerAbstract->addProperty('abstract', (string)$address->getMetaAbstract());
        //
        /** @var MetaTagManagerInterface $metaTagManagerKeywords */
        $metaTagManagerKeywords = $metaTagManagerRegistry->getManagerForProperty('keywords');
        $metaTagManagerKeywords->addProperty('keywords', (string)$address->getMetaKeywords());
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgTitle */
        $metaTagManagerOgTitle = $metaTagManagerRegistry->getManagerForProperty('og:title');
        $metaTagManagerOgTitle->addProperty('og:title', $htmlTitle);
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgDescription */
        $metaTagManagerOgDescription = $metaTagManagerRegistry->getManagerForProperty('og:description');
        $metaTagManagerOgDescription->addProperty('og:description', $description);
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgType */
        $metaTagManagerOgType = $metaTagManagerRegistry->getManagerForProperty('og:type');
        $metaTagManagerOgType->addProperty('og:type', 'WebPage');
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgSiteName */
        $metaTagManagerOgSiteName = $metaTagManagerRegistry->getManagerForProperty('og:site_name');
        $metaTagManagerOgSiteName->addProperty('og:site_name', $siteName);
        if ($siteName === '{$themes.configuration.siteName}') {
            $messageBody = 'Please set TypoScript constant {$themes.configuration.siteName} with sitename for og:site_name';
            $messageTitle = 'Error';
            $this->addFlashMessage($messageBody, $messageTitle, AbstractMessage::ERROR);
        }
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgUrl */
        $metaTagManagerOgUrl = $metaTagManagerRegistry->getManagerForProperty('og:url');
        $metaTagManagerOgUrl->addProperty('og:url', GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'));
        //
        // Preview image
        $previewImages = $address->getImagesPreviewOnly();
        if (count($previewImages) > 0) {
            /** @var FileReference $previewImage */
            $previewImage = $previewImages[0];
            /** @var ImageService $imageService */
            $imageService = GeneralUtility::makeInstance(ImageService::class);
            $image = $imageService->getImage($previewImage, $previewImage, true);
            //
            // Cropping
            if ($image->hasProperty('crop') && $image->getProperty('crop')) {
                $cropString = $image->getProperty('crop');
            }
            $cropVariantCollection = CropVariantCollection::create((string)($cropString ?? ''));
            $cropVariant = 'default';
            $cropArea = $cropVariantCollection->getCropArea($cropVariant);
            $processingInstructions = [
                'width' => 600,
                'crop' => $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($image),
            ];
            //
            $processedImage = $imageService->applyProcessingInstructions($image, $processingInstructions);
            $imageUrl = $imageService->getImageUri($processedImage, true);
            //
            /** @var MetaTagManagerInterface $metaTagManagerOgImage */
            $metaTagManagerOgImage = $metaTagManagerRegistry->getManagerForProperty('og:image');
            $metaTagManagerOgImage->addProperty('og:image', $imageUrl);
        }
        //
        /** @var PageTitleProvider $seoTitlePageTitleProvider */
        $seoTitlePageTitleProvider = GeneralUtility::makeInstance(PageTitleProvider::class);
        $seoTitlePageTitleProvider->setTitle($htmlTitle);
    }

    /**
     * @return ResponseInterface
     */
    public function groupTeaserAction(): ResponseInterface
    {
        $allowedGroups = [];
        if (!empty($this->settings['list']['allowedGroups'])) {
            $allowedGroups = explode(',', $this->settings['list']['allowedGroups']);
        }
        /** @var QueryResult $groups */
        $groups = $this->addressGroupRepository->findAllByAllowedGroups(
            $allowedGroups,
            $this->settings['list']['group']['sortBy'] ?? '',
            $this->settings['list']['group']['sortOrder'] ?? ''
        );
        $this->view->assign('groups', $groups);

        return $this->htmlResponse();
    }

    /**
     * Prepares the filter fields
     *
     * @return void
     */
    protected function explodeListFilterFields(): void
    {
        if (empty($this->settings['list']['filterFields'])) {
            $this->settings['list']['filterFields'] = [];
        } else {
            $this->settings['list']['showFilter'] = true;
            $this->settings['list']['filterFields'] = explode(',', $this->settings['list']['filterFields']);
            $this->settings['list']['filterFields'] = array_combine(
                $this->settings['list']['filterFields'],
                $this->settings['list']['filterFields']
            );
        }
    }
}
