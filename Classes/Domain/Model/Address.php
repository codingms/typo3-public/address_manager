<?php

namespace CodingMs\AddressManager\Domain\Model;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Domain\Model\Traits\EndtimeTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\StarttimeTrait;
use CodingMs\AddressManager\Service\GeoLocationService;
use CodingMs\Modules\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Address
 */
class Address extends AbstractEntity
{
    use EndtimeTrait;
    use StarttimeTrait;

    /**
     * @var \DateTime
     */
    protected \DateTime$creationDate;

    /**
     * @var ?\DateTime
     */
    protected ?\DateTime$modificationDate;

    /**
     * @var string
     */
    protected string $name = '';

    /**
     * @var string
     */
    protected string $slug = '';

    /**
     * @var bool
     */
    protected bool $hidden = true;

    /**
     * @var bool
     */
    protected bool $deleted = true;

    /**
     * @var FrontendUser
     */
    protected FrontendUser $frontendUser;

    /**
     * @var string
     */
    protected string $gender = '';

    /**
     * @var string
     */
    protected string $firstName = '';

    /**
     * @var string
     */
    protected string $middleName = '';

    /**
     * @var string
     */
    protected string $lastName = '';

    /**
     * @var ?\DateTime
     */
    protected ?\DateTime $birthday;

    /**
     * @var string
     */
    protected string $title = '';

    /**
     * @var string
     */
    protected string $email = '';

    /**
     * @var string
     */
    protected string $phone = '';

    /**
     * @var string
     */
    protected string $mobile = '';

    /**
     * @var string
     */
    protected string $www = '';

    /**
     * @var string
     */
    protected string $address = '';

    /**
     * @var string
     */
    protected string $building = '';

    /**
     * @var string
     */
    protected string $room = '';

    /**
     * @var string
     */
    protected string $company = '';

    /**
     * @var string
     */
    protected string $city = '';

    /**
     * @var string
     */
    protected string $postalCode = '';

    /**
     * @var string
     */
    protected string $region = '';

    /**
     * @var string
     */
    protected string$country = '';

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $images;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $files;

    /**
     * @var string
     */
    protected string $fax = '';

    /**
     * @var string
     */
    protected string $description = '';

    /**
     * @var string
     */
    protected string $mapLatitude = '';

    /**
     * @var string
     */
    protected string $mapLongitude = '';

    /**
     * @var int
     */
    protected int $mapZoom = 0;

    /**
     * @var string
     */
    protected $mapTooltip = '';

    /**
     * @var string
     */
    protected string $mapLink = '';

    /**
     * @var string
     */
    protected string $mapMarker = 'default';

    /**
     * @var float
     */
    protected float $distance = 0;

    /**
     * @var string
     */
    protected string $directions = '';

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\AddressGroup>
     */
    protected $groups;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\AddressPosition>
     */
    protected $positions;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\AddressOrganisation>
     */
    protected $organisations;

    /**
     * Custom attribute 1
     * @var string
     */
    protected string $attribute1 = '';

    /**
     * Custom attribute 2
     * @var string
     */
    protected string $attribute2 = '';

    /**
     * Custom attribute 3
     * @var string
     */
    protected string $attribute3 = '';

    /**
     * Custom attribute 4
     * @var string
     */
    protected string $attribute4 = '';

    /**
     * Custom attribute 5
     * @var string
     */
    protected string $attribute5 = '';

    /**
     * Custom attribute 6
     * @var string
     */
    protected string $attribute6 = '';

    /**
     * Custom attribute 7
     * @var string
     */
    protected string $attribute7 = '';

    /**
     * Custom attribute 8
     * @var string
     */
    protected string $attribute8 = '';

    /**
     * Custom attribute 9
     * @var string
     */
    protected string $attribute9 = '';

    /**
     * Custom attribute 10
     * @var string
     */
    protected string $attribute10 = '';

    /**
     * Custom attribute 11
     * @var string
     */
    protected string $attribute11 = '';

    /**
     * Custom attribute 12
     * @var string
     */
    protected string $attribute12 = '';

    /**
     * Custom attribute 13
     * @var string
     */
    protected string $attribute13 = '';

    /**
     * Custom attribute 14
     * @var string
     */
    protected string $attribute14 = '';

    /**
     * Custom attribute 15
     * @var string
     */
    protected string $attribute15 = '';

    /**
     * Custom attribute 16
     * @var string
     */
    protected string $attribute16 = '';

    /**
     * Custom attribute 17
     * @var string
     */
    protected string $attribute17 = '';

    /**
     * @var string
     */
    protected string $htmlTitle = '';

    /**
     * @var string
     */
    protected string $metaAbstract = '';

    /**
     * @var string
     */
    protected string $metaDescription = '';

    /**
     * @var string
     */
    protected string $metaKeywords = '';

    /**
     * @var string
     */
    protected string $facebook = '';


    /**
     * @var string
     */
    protected string $linkedin = '';

    /**
     * @var string
     */
    protected string $instagram = '';

    /**
     * @var string
     */
    protected string $twitter = '';

    /**
     * @var string
     */
    protected string $youtube = '';

    /**
     * @var string
     */
    protected string $openingHoursMondayOpens = '';

    /**
     * @var string
     */
    protected string $openingHoursMondayCloses = '';

    /**
     * @var string
     */
    protected string $openingHoursTuesdayOpens = '';

    /**
     * @var string
     */
    protected string $openingHoursTuesdayCloses = '';

    /**
     * @var string
     */
    protected string $openingHoursWednesdayOpens = '';

    /**
     * @var string
     */
    protected string $openingHoursWednesdayCloses = '';

    /**
     * @var string
     */
    protected string $openingHoursThursdayOpens = '';

    /**
     * @var string
     */
    protected string $openingHoursThursdayCloses = '';

    /**
     * @var string
     */
    protected string $openingHoursFridayOpens = '';

    /**
     * @var string
     */
    protected string $openingHoursFridayCloses = '';

    /**
     * @var string
     */
    protected string $openingHoursSaturdayOpens = '';

    /**
     * @var string
     */
    protected string $openingHoursSaturdayCloses = '';

    /**
     * @var string
     */
    protected string $openingHoursSundayOpens = '';

    /**
     * @var string
     */
    protected string $openingHoursSundayCloses = '';

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\Address>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $addresses;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     *  Initializes all ObjectStorage properties
     *  Do not modify this method!
     *  It will be rewritten on each save in the extension builder
     *  You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects(): void
    {
        $this->files = new ObjectStorage();
        $this->images = new ObjectStorage();
        $this->groups = new ObjectStorage();
        $this->positions = new ObjectStorage();
        $this->organisations = new ObjectStorage();
        $this->addresses = new ObjectStorage();
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }

    /**
     * @param \DateTime $creationDate
     * @return void
     */
    public function setCreationDate(\DateTime$creationDate): void
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getModificationDate(): ?\DateTime
    {
        return $this->modificationDate;
    }

    /**
     * @param \DateTime $modificationDate
     * @return void
     */
    public function setModificationDate(\DateTime$modificationDate): void
    {
        $this->modificationDate = $modificationDate;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return bool
     */
    public function getHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @return bool
     */
    public function isHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     * @return void
     */
    public function setHidden(bool $hidden): void
    {
        $this->hidden = $hidden;
    }

    /**
     * @return bool
     */
    public function getDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     * @return void
     */
    public function setDeleted(bool $deleted): void
    {
        $this->deleted = $deleted;
    }

    /**
     * @return ?FrontendUser
     */
    public function getFrontendUser(): ?FrontendUser
    {
        return $this->frontendUser;
    }

    /**
     * @param FrontendUser $frontendUser
     * @return void
     */
    public function setFrontendUser(FrontendUser $frontendUser): void
    {
        $this->frontendUser = $frontendUser;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName(string$name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return void
     */
    public function setGender(string$gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return void
     */
    public function setFirstName(string$firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     * @return void
     */
    public function setMiddleName(string$middleName): void
    {
        $this->middleName = $middleName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return void
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return \DateTime|null
     */
    public function getBirthday(): ?\DateTime
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime $birthday
     * @return void
     */
    public function setBirthday(\DateTime $birthday): void
    {
        $this->birthday = $birthday;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return void
     */
    public function setTitle(string$title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return void
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return void
     */
    public function setPhone(string$phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     * @return void
     */
    public function setMobile(string$mobile): void
    {
        $this->mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getWww(): string
    {
        return $this->www;
    }

    /**
     * @param string $www
     * @return void
     */
    public function setWww(string$www): void
    {
        $this->www = $www;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return void
     */
    public function setAddress(string$address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getBuilding(): string
    {
        return $this->building;
    }

    /**
     * @param string $building
     * @return void
     */
    public function setBuilding(string$building): void
    {
        $this->building = $building;
    }

    /**
     * @return string
     */
    public function getRoom(): string
    {
        return $this->room;
    }

    /**
     * @param string $room
     * @return void
     */
    public function setRoom(string$room): void
    {
        $this->room = $room;
    }

    /**
     * @return string
     */
    public function getCompany(): string
    {
        return $this->company;
    }

    /**
     * @param string $company
     * @return void
     */
    public function setCompany(string$company): void
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return void
     */
    public function setCity(string$city):void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     * @return void
     */
    public function setPostalCode(string$postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return string
     */
    public function getRegion(): string
    {
        return $this->region;
    }

    /**
     * @param string $region
     * @return void
     */
    public function setRegion(string$region): void
    {
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return void
     */
    public function setCountry(string$country): void
    {
        $this->country = $country;
    }

    /**
     * @param FileReference $image
     * @return void
     */
    public function addImage(FileReference $image): void
    {
        $this->images->attach($image);
    }

    /**
     * @param FileReference $imageToRemove
     * @return void
     */
    public function removeImage(FileReference $imageToRemove): void
    {
        $this->images->detach($imageToRemove);
    }

    /**
     * @return ObjectStorage<FileReference>
     */
    public function getImages(): ObjectStorage
    {
        if ($this->images instanceof LazyLoadingProxy) {
            $this->images->_loadRealInstance();
        }
        return $this->images;
    }

    /**
     * @param ObjectStorage $images
     * @return void
     */
    public function setImages(ObjectStorage $images)
    {
        $this->images = $images;
    }

    /**
     * Returns all images marked as preview
     * @return array<int, mixed> $images
     */
    public function getImagesPreviewOnly()
    {
        $images = [];
        if ($this->getImages()->count() >0) {
            /** @var \CodingMs\AddressManager\Domain\Model\FileReference $mediaItem */
            foreach ($this->getImages() as $mediaItem) {
                if ($mediaItem->getOriginalResource()->getProperty('preview')) {
                    $images[] = $mediaItem;
                }
            }
        }
        return $images;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $file
     * @return void
     */
    public function addFile(\TYPO3\CMS\Extbase\Domain\Model\FileReference $file)
    {
        $this->files->attach($file);
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $fileToRemove
     * @return void
     */
    public function removeFile(\TYPO3\CMS\Extbase\Domain\Model\FileReference $fileToRemove)
    {
        $this->files->detach($fileToRemove);
    }

    /**
     * @return ObjectStorage
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param ObjectStorage $files
     * @return void
     */
    public function setFiles(ObjectStorage $files)
    {
        $this->files = $files;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     * @return void
     */
    public function setFax(string $fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return void
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return float|string
     */
    public function getMapLatitude()
    {
        return GeoLocationService::validateLatitude($this->mapLatitude);
    }

    /**
     * @param string $latitude
     * @return void
     */
    public function setMapLatitude(string $latitude)
    {
        $this->mapLatitude = (string)GeoLocationService::validateLatitude($latitude);
    }

    /**
     * @return float|string
     */
    public function getMapLongitude()
    {
        return GeoLocationService::validateLongitude($this->mapLongitude);
    }

    /**
     * @param string $longitude
     * @return void
     */
    public function setMapLongitude(string $longitude)
    {
        $this->mapLongitude = (string)GeoLocationService::validateLongitude($longitude);;
    }

    /**
     * Returns the mapZoom
     *
     * @return int $mapZoom
     */
    public function getMapZoom()
    {
        return $this->mapZoom;
    }

    /**
     * @param int $mapZoom
     * @return void
     */
    public function setMapZoom(int $mapZoom)
    {
        $this->mapZoom = $mapZoom;
    }

    /**
     * Returns the mapTooltip
     *
     * @return string $mapTooltip
     */
    public function getMapTooltip()
    {
        return $this->mapTooltip;
    }

    /**
     * @param string $mapTooltip
     * @return void
     */
    public function setMapTooltip(string$mapTooltip)
    {
        $this->mapTooltip = $mapTooltip;
    }

    /**
     * Returns the mapLink
     *
     * @return string $mapLink
     */
    public function getMapLink()
    {
        return $this->mapLink;
    }

    /**
     * @param string $mapLink
     * @return void
     */
    public function setMapLink(string$mapLink)
    {
        $this->mapLink = $mapLink;
    }

    /**
     * @return string
     */
    public function getMapMarker()
    {
        if (trim($this->mapMarker) === '') {
            $this->mapMarker = 'default';
        }
        return $this->mapMarker;
    }

    /**
     * @param string $mapMarker
     * @return void
     */
    public function setMapMarker(string$mapMarker)
    {
        $this->mapMarker = $mapMarker;
    }

    /**
     * @return float
     */
    public function getDistance()
    {
        // Attention:
        // Ensure that distance is from type float!
        return (float)$this->distance;
    }

    /**
     * @param float $distance
     * @return void
     */
    public function setDistance(float$distance)
    {
        $this->distance = $distance;
    }

    /**
     * Returns the directions
     *
     * @return string $directions
     */
    public function getDirections()
    {
        return $this->directions;
    }

    /**
     * @param string $directions
     * @return void
     */
    public function setDirections(string$directions)
    {
        $this->directions = $directions;
    }

    /**
     * @param AddressGroup $group
     * @return void
     */
    public function addGroup(AddressGroup $group)
    {
        $this->groups->attach($group);
    }

    /**
     * @param AddressGroup $groupToRemove
     * @return void
     */
    public function removeGroup(AddressGroup $groupToRemove)
    {
        $this->groups->detach($groupToRemove);
    }

    /**
     * @return ObjectStorage
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param ObjectStorage $groups
     * @return void
     */
    public function setGroups(ObjectStorage $groups)
    {
        $this->groups = $groups;
    }

    /**
     * @return string
     */
    public function getGroupsString()
    {
        $groups = [];
        /** @var \CodingMs\AddressManager\Domain\Model\AddressGroup $group */
        foreach ($this->getGroups() as $group) {
            $groups[] = $group->getTitle();
        }
        return implode(', ', $groups);
    }

    /**
     * @param AddressPosition $position
     * @return void
     */
    public function addPosition(AddressPosition $position)
    {
        $this->positions->attach($position);
    }

    /**
     * @param AddressPosition $positionToRemove
     * @return void
     */
    public function removePosition(AddressPosition $positionToRemove)
    {
        $this->positions->detach($positionToRemove);
    }

    /**
     * @return ObjectStorage
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * @param ObjectStorage $positions
     * @return void
     */
    public function setPositions(ObjectStorage $positions)
    {
        $this->positions = $positions;
    }

    /**
     * @return string
     */
    public function getPositionsString()
    {
        $positions = [];
        /** @var \CodingMs\AddressManager\Domain\Model\AddressPosition $position */
        foreach ($this->getPositions() as $position) {
            $positions[] = $position->getTitle();
        }
        return implode(', ', $positions);
    }

    /**
     * @param AddressOrganisation $organisation
     * @return void
     */
    public function addOrganisation(AddressOrganisation $organisation)
    {
        $this->organisations->attach($organisation);
    }

    /**
     * @param AddressOrganisation $organisationToRemove
     * @return void
     */
    public function removeOrganisation(AddressOrganisation $organisationToRemove)
    {
        $this->organisations->detach($organisationToRemove);
    }

    /**
     * @return ObjectStorage
     */
    public function getOrganisations()
    {
        return $this->organisations;
    }

    /**
     * @param ObjectStorage $organisations
     * @return void
     */
    public function setOrganisations(ObjectStorage $organisations)
    {
        $this->organisations = $organisations;
    }

    /**
     * @return string
     */
    public function getOrganisationsString()
    {
        $organisations = [];
        /** @var \CodingMs\AddressManager\Domain\Model\AddressOrganisation $organisation */
        foreach ($this->getOrganisations() as $organisation) {
            $organisations[] = $organisation->getTitle();
        }
        return implode(', ', $organisations);
    }

    /**
     * @param bool $withoutDescription
     * @return array<string, mixed>
     */
    public function toArray(bool $withoutDescription=false)
    {
        $array = [];
        $array['pid'] = $this->getPid();
        $array['uid'] = $this->getUid();
        $array['name'] = $this->getName();
        if (!$withoutDescription) {
            $array['description'] = $this->getDescription();
        }
        //
        $array['groups'] = [];
        $groups = $this->getGroups();
        if (count($groups) > 0) {
            /** @var \CodingMs\AddressManager\Domain\Model\AddressGroup $group */
            foreach ($groups as $group) {
                $array['groups'][] = $group->toArray($withoutDescription);
            }
        }
        //
        $array['positions'] = [];
        $positions = $this->getPositions();
        if (count($positions) > 0) {
            /** @var \CodingMs\AddressManager\Domain\Model\AddressPosition $position */
            foreach ($positions as $position) {
                $array['positions'][] = $position->toArray($withoutDescription);
            }
        }
        //
        $array['organisations'] = [];
        $organisations = $this->getOrganisations();
        if (count($organisations) > 0) {
            /** @var \CodingMs\AddressManager\Domain\Model\AddressOrganisation $organisation */
            foreach ($organisations as $organisation) {
                $array['organisations'][] = $organisation->toArray($withoutDescription);
            }
        }
        return $array;
    }

    /**
     * Returns the categories as a String
     *
     * @return string
     */
    public function getCssClasses()
    {
        $classesArray = [];
        // Get all groups
        $groups = $this->getGroups();
        if (!empty($groups)) {
            /** @var \CodingMs\AddressManager\Domain\Model\AddressGroup $group */
            foreach ($groups as $group) {
                $classesArray[] = 'address-group-' . $group->getUid();
            }
        }
        // Get all positions
        $positions = $this->getPositions();
        if (!empty($positions)) {
            /** @var \CodingMs\AddressManager\Domain\Model\AddressPosition $position */
            foreach ($positions as $position) {
                $classesArray[] = 'address-position-' . $position->getUid();
            }
        }
        // Get all organisations
        $organisations = $this->getOrganisations();
        if (!empty($organisations)) {
            /** @var \CodingMs\AddressManager\Domain\Model\AddressOrganisation $organisation */
            foreach ($organisations as $organisation) {
                $classesArray[] = 'address-organisation-' . $organisation->getUid();
            }
        }
        return implode(' ', $classesArray);
    }

    /**
     * @return string
     */
    public function getAttribute1(): ?string
    {
        return $this->attribute1;
    }

    /**
     * @param string $attribute1
     */
    public function setAttribute1(string $attribute1): void
    {
        $this->attribute1 = $attribute1;
    }

    /**
     * @return string
     */
    public function getAttribute2(): ?string
    {
        return $this->attribute2;
    }

    /**
     * @param string $attribute2
     */
    public function setAttribute2(string $attribute2): void
    {
        $this->attribute2 = $attribute2;
    }

    /**
     * @return string
     */
    public function getAttribute3(): ?string
    {
        return $this->attribute3;
    }

    /**
     * @param string $attribute3
     */
    public function setAttribute3(string $attribute3): void
    {
        $this->attribute3 = $attribute3;
    }

    /**
     * @return string
     */
    public function getAttribute4(): ?string
    {
        return $this->attribute4;
    }

    /**
     * @param string $attribute4
     */
    public function setAttribute4(string $attribute4): void
    {
        $this->attribute4 = $attribute4;
    }

    /**
     * @return string
     */
    public function getAttribute5(): ?string
    {
        return $this->attribute5;
    }

    /**
     * @param string $attribute5
     */
    public function setAttribute5(string $attribute5): void
    {
        $this->attribute5 = $attribute5;
    }

    /**
     * @return string
     */
    public function getAttribute6(): ?string
    {
        return $this->attribute6;
    }

    /**
     * @param string $attribute6
     */
    public function setAttribute6(string $attribute6): void
    {
        $this->attribute6 = $attribute6;
    }

    /**
     * @return string
     */
    public function getAttribute7(): ?string
    {
        return $this->attribute7;
    }

    /**
     * @param string $attribute7
     */
    public function setAttribute7(string $attribute7): void
    {
        $this->attribute7 = $attribute7;
    }

    /**
     * @return string
     */
    public function getAttribute8(): ?string
    {
        return $this->attribute8;
    }

    /**
     * @param string $attribute8
     */
    public function setAttribute8(string $attribute8): void
    {
        $this->attribute8 = $attribute8;
    }

    /**
     * @return string
     */
    public function getAttribute9(): ?string
    {
        return $this->attribute9;
    }

    /**
     * @param string $attribute9
     */
    public function setAttribute9(string $attribute9): void
    {
        $this->attribute9 = $attribute9;
    }

    /**
     * @return string
     */
    public function getAttribute10(): ?string
    {
        return $this->attribute10;
    }

    /**
     * @param string $attribute10
     */
    public function setAttribute10(string $attribute10): void
    {
        $this->attribute10 = $attribute10;
    }

    /**
     * @return string
     */
    public function getAttribute11(): ?string
    {
        return $this->attribute11;
    }

    /**
     * @param string $attribute11
     */
    public function setAttribute11(string $attribute11): void
    {
        $this->attribute11 = $attribute11;
    }

    /**
     * @return string
     */
    public function getAttribute12(): ?string
    {
        return $this->attribute12;
    }

    /**
     * @param string $attribute12
     */
    public function setAttribute12(string $attribute12): void
    {
        $this->attribute12 = $attribute12;
    }

    /**
     * @return string
     */
    public function getAttribute13(): ?string
    {
        return $this->attribute13;
    }

    /**
     * @param string $attribute13
     */
    public function setAttribute13(string $attribute13): void
    {
        $this->attribute13 = $attribute13;
    }

    /**
     * @return string
     */
    public function getAttribute14(): ?string
    {
        return $this->attribute14;
    }

    /**
     * @param string $attribute14
     */
    public function setAttribute14(string $attribute14): void
    {
        $this->attribute14 = $attribute14;
    }

    /**
     * @return string
     */
    public function getAttribute15(): ?string
    {
        return $this->attribute15;
    }

    /**
     * @param string $attribute15
     */
    public function setAttribute15(string $attribute15): void
    {
        $this->attribute15 = $attribute15;
    }

    /**
     * @return string
     */
    public function getAttribute16(): ?string
    {
        return $this->attribute16;
    }

    /**
     * @param string $attribute16
     */
    public function setAttribute16(string $attribute16): void
    {
        $this->attribute16 = $attribute16;
    }

    /**
     * @return string
     */
    public function getAttribute17(): ?string
    {
        return $this->attribute17;
    }

    /**
     * @param string $attribute17
     */
    public function setAttribute17(string $attribute17): void
    {
        $this->attribute17 = $attribute17;
    }

    /**
     * @return string
     */
    public function getHtmlTitle(): ?string
    {
        return $this->htmlTitle;
    }

    /**
     * @param string $htmlTitle
     */
    public function setHtmlTitle(string $htmlTitle): void
    {
        $this->htmlTitle = $htmlTitle;
    }

    /**
     * @return string
     */
    public function getMetaAbstract(): ?string
    {
        return $this->metaAbstract;
    }

    /**
     * @param string $metaAbstract
     */
    public function setMetaAbstract(string $metaAbstract): void
    {
        $this->metaAbstract = $metaAbstract;
    }

    /**
     * @return string
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription(string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return string
     */
    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    /**
     * @param string $metaKeywords
     */
    public function setMetaKeywords(string $metaKeywords): void
    {
        $this->metaKeywords = $metaKeywords;
    }

    /**
     * @return string
     */
    public function getFacebook(): string
    {
        return $this->facebook;
    }

    /**
     * @param string $facebook
     */
    public function setFacebook(string $facebook): void
    {
        $this->facebook = $facebook;
    }

    /**
     * @return string
     */
    public function getLinkedin(): string
    {
        return $this->linkedin;
    }

    /**
     * @param string $linkedin
     */
    public function setLinkedin(string $linkedin): void
    {
        $this->linkedin = $linkedin;
    }

    /**
     * @return string
     */
    public function getInstagram(): string
    {
        return $this->instagram;
    }

    /**
     * @param string $instagram
     */
    public function setInstagram(string $instagram): void
    {
        $this->instagram = $instagram;
    }

    /**
     * @return string
     */
    public function getTwitter(): string
    {
        return $this->twitter;
    }

    /**
     * @param string $twitter
     */
    public function setTwitter(string $twitter): void
    {
        $this->twitter = $twitter;
    }

    /**
     * @return string
     */
    public function getYoutube(): string
    {
        return $this->youtube;
    }

    /**
     * @param string $youtube
     */
    public function setYoutube(string $youtube): void
    {
        $this->youtube = $youtube;
    }

    /**
     * @return string
     */
    public function getOpeningHoursMondayOpens(): string
    {
        return $this->openingHoursMondayOpens;
    }

    /**
     * @param string $openingHoursMondayOpens
     */
    public function setOpeningHoursMondayOpens(string $openingHoursMondayOpens): void
    {
        $this->openingHoursMondayOpens = $openingHoursMondayOpens;
    }

    /**
     * @return string
     */
    public function getOpeningHoursMondayCloses(): string
    {
        return $this->openingHoursMondayCloses;
    }

    /**
     * @param string $openingHoursMondayCloses
     */
    public function setOpeningHoursMondayCloses(string $openingHoursMondayCloses): void
    {
        $this->openingHoursMondayCloses = $openingHoursMondayCloses;
    }

    /**
     * @return string
     */
    public function getOpeningHoursTuesdayOpens(): string
    {
        return $this->openingHoursTuesdayOpens;
    }

    /**
     * @param string $openingHoursTuesdayOpens
     */
    public function setOpeningHoursTuesdayOpens(string $openingHoursTuesdayOpens): void
    {
        $this->openingHoursTuesdayOpens = $openingHoursTuesdayOpens;
    }

    /**
     * @return string
     */
    public function getOpeningHoursTuesdayCloses(): string
    {
        return $this->openingHoursTuesdayCloses;
    }

    /**
     * @param string $openingHoursTuesdayCloses
     */
    public function setOpeningHoursTuesdayCloses(string $openingHoursTuesdayCloses): void
    {
        $this->openingHoursTuesdayCloses = $openingHoursTuesdayCloses;
    }

    /**
     * @return string
     */
    public function getOpeningHoursWednesdayOpens(): string
    {
        return $this->openingHoursWednesdayOpens;
    }

    /**
     * @param string $openingHoursWednesdayOpens
     */
    public function setOpeningHoursWednesdayOpens(string $openingHoursWednesdayOpens): void
    {
        $this->openingHoursWednesdayOpens = $openingHoursWednesdayOpens;
    }

    /**
     * @return string
     */
    public function getOpeningHoursWednesdayCloses(): string
    {
        return $this->openingHoursWednesdayCloses;
    }

    /**
     * @param string $openingHoursWednesdayCloses
     */
    public function setOpeningHoursWednesdayCloses(string $openingHoursWednesdayCloses): void
    {
        $this->openingHoursWednesdayCloses = $openingHoursWednesdayCloses;
    }

    /**
     * @return string
     */
    public function getOpeningHoursThursdayOpens(): string
    {
        return $this->openingHoursThursdayOpens;
    }

    /**
     * @param string $openingHoursThursdayOpens
     */
    public function setOpeningHoursThursdayOpens(string $openingHoursThursdayOpens): void
    {
        $this->openingHoursThursdayOpens = $openingHoursThursdayOpens;
    }

    /**
     * @return string
     */
    public function getOpeningHoursThursdayCloses(): string
    {
        return $this->openingHoursThursdayCloses;
    }

    /**
     * @param string $openingHoursThursdayCloses
     */
    public function setOpeningHoursThursdayCloses(string $openingHoursThursdayCloses): void
    {
        $this->openingHoursThursdayCloses = $openingHoursThursdayCloses;
    }

    /**
     * @return string
     */
    public function getOpeningHoursFridayOpens(): string
    {
        return $this->openingHoursFridayOpens;
    }

    /**
     * @param string $openingHoursFridayOpens
     */
    public function setOpeningHoursFridayOpens(string $openingHoursFridayOpens): void
    {
        $this->openingHoursFridayOpens = $openingHoursFridayOpens;
    }

    /**
     * @return string
     */
    public function getOpeningHoursFridayCloses(): string
    {
        return $this->openingHoursFridayCloses;
    }

    /**
     * @param string $openingHoursFridayCloses
     */
    public function setOpeningHoursFridayCloses(string $openingHoursFridayCloses): void
    {
        $this->openingHoursFridayCloses = $openingHoursFridayCloses;
    }

    /**
     * @return string
     */
    public function getOpeningHoursSaturdayOpens(): string
    {
        return $this->openingHoursSaturdayOpens;
    }

    /**
     * @param string $openingHoursSaturdayOpens
     */
    public function setOpeningHoursSaturdayOpens(string $openingHoursSaturdayOpens): void
    {
        $this->openingHoursSaturdayOpens = $openingHoursSaturdayOpens;
    }

    /**
     * @return string
     */
    public function getOpeningHoursSaturdayCloses(): string
    {
        return $this->openingHoursSaturdayCloses;
    }

    /**
     * @param string $openingHoursSaturdayCloses
     */
    public function setOpeningHoursSaturdayCloses(string $openingHoursSaturdayCloses): void
    {
        $this->openingHoursSaturdayCloses = $openingHoursSaturdayCloses;
    }

    /**
     * @return string
     */
    public function getOpeningHoursSundayOpens(): string
    {
        return $this->openingHoursSundayOpens;
    }

    /**
     * @param string $openingHoursSundayOpens
     */
    public function setOpeningHoursSundayOpens(string $openingHoursSundayOpens): void
    {
        $this->openingHoursSundayOpens = $openingHoursSundayOpens;
    }

    /**
     * @return string
     */
    public function getOpeningHoursSundayCloses(): string
    {
        return $this->openingHoursSundayCloses;
    }

    /**
     * @param string $openingHoursSundayCloses
     */
    public function setOpeningHoursSundayCloses(string $openingHoursSundayCloses): void
    {
        $this->openingHoursSundayCloses = $openingHoursSundayCloses;
    }

    /**
     * Returns the addresses
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\Address> $addresses
     */
    public function getAddresses()
    {
        return $this->addresses;
    }
    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\Address> $addresses
     */
    public function setAddresses(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $addresses): void
    {
        $this->addresses = $addresses;
    }
}
