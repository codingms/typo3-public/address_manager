<?php

namespace CodingMs\AddressManager\Domain\Repository;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AddressManager\Domain\Model\Address;
use CodingMs\AddressManager\Domain\Model\AddressGroup;
use CodingMs\AddressManager\Domain\Model\AddressOrganisation;
use CodingMs\AddressManager\Domain\Model\AddressPosition;
use Doctrine\DBAL\Result;
use PDO;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\LanguageAspect;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;
use TYPO3\CMS\Extbase\Persistence\Generic\QueryResult;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class AddressRepository extends Repository
{
    /**
     * Default ordering by sorting, because select box entries can be sorted easily
     * @var array<non-empty-string,QueryInterface::ORDER_*>
     */
    protected $defaultOrderings = [
        'sorting' => QueryInterface::ORDER_ASCENDING
    ];

    /**
     * @return DataMapper
     */
    protected function getDataMapper()
    {
        /** @var DataMapper $dataMapper */
        $dataMapper = GeneralUtility::makeInstance(DataMapper::class);
        return $dataMapper;
    }

    /**
     * @param array<mixed> $filter
     * @param string $sortBy Sort by field
     * @param string $order Sorting order ASC or DESC
     * @param int $offset Offset of address items
     * @param int $limit Limit of address items
     * @return array<mixed>|QueryResultInterface
     * @throws InvalidQueryException
     */
    public function findAllByFilter(array $filter = [], $sortBy = 'name', $order = 'ASC', $offset = 0, $limit = 0)
    {
        $query = $this->createQuery();
        /** By using the json API, the record storage isn't available! */
        $query->getQuerySettings()->setRespectStoragePage(false);
        if (!empty($filter)) {
            $constraints = $this->getConstraints($query, $filter);
            if (!empty($constraints)) {
                if (count($constraints) > 1) {
                    $query->matching(
                        $query->logicalAnd(...$constraints)
                    );
                } else {
                    $query->matching($constraints[0]);
                }
            }
        }
        // Sort by start date
        if ($order === 'ASC') {
            $orderings = [$sortBy => QueryInterface::ORDER_ASCENDING];
            if ($sortBy === 'genderThenName') {
                $orderings = ['gender' => QueryInterface::ORDER_ASCENDING, 'name' => QueryInterface::ORDER_ASCENDING];
            }
            if ($sortBy === 'cityThenName') {
                $orderings = ['city' => QueryInterface::ORDER_ASCENDING, 'name' => QueryInterface::ORDER_ASCENDING];
            }
            if ($sortBy === 'lastnameThenFirstname') {
                $orderings = ['lastName' => QueryInterface::ORDER_ASCENDING, 'firstName' => QueryInterface::ORDER_ASCENDING];
            }
            $query->setOrderings($orderings);
        } else {
            $orderings = [$sortBy => QueryInterface::ORDER_DESCENDING];
            if ($sortBy === 'genderThenName') {
                $orderings = ['gender' => QueryInterface::ORDER_DESCENDING, 'name' => QueryInterface::ORDER_DESCENDING];
            }
            if ($sortBy === 'cityThenName') {
                $orderings = ['city' => QueryInterface::ORDER_DESCENDING, 'name' => QueryInterface::ORDER_DESCENDING];
            }
            if ($sortBy === 'lastnameThenFirstname') {
                $orderings = ['lastName' => QueryInterface::ORDER_DESCENDING, 'firstName' => QueryInterface::ORDER_DESCENDING];
            }
            $query->setOrderings($orderings);
        }
        // Use the offset, if available
        if ($offset > 0) {
            $query->setOffset($offset);
        }
        // Use the limit, if available
        if ($limit > 0) {
            $query->setLimit($limit);
        }
        return $query->execute();
    }

    /**
     * @return array<int, mixed>
     * @throws \Doctrine\DBAL\Exception
     */
    public function findAllAddress(): array
    {
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_addressmanager_domain_model_address');
        $queryBuilder->select('*')
            ->from('tx_addressmanager_domain_model_address');

        $result = $queryBuilder->executeQuery();
        return $result->fetchAllAssociative();
    }

    /**
     * @param array<mixed> $filter
     * @return int
     * @throws InvalidQueryException
     */
    public function countAllByFilter(array $filter = [])
    {
        $query = $this->createQuery();
        /** By using the json API, the record storage isn't available! */
        $query->getQuerySettings()->setRespectStoragePage(false);
        if (!empty($filter)) {
            $constraints = $this->getConstraints($query, $filter);
            if (!empty($constraints)) {
                $query->matching(
                    $query->logicalAnd(...$constraints)
                );
            }
        }
        return $query->execute()->count();
    }

    /**
     * @param QueryInterface $query
     * @param array<mixed> $filter
     * @return array<int, mixed>
     * @throws InvalidQueryException
     */
    public function getConstraints(QueryInterface$query, array $filter)
    {
        $constraints = [];
        if (!empty($filter)) {
            foreach ($filter as $filterKey => $filterItem) {
                if ($filterItem instanceof AddressGroup) {
                    $constraints[] = $query->contains('groups', $filterItem);
                }
                if ($filterItem instanceof AddressPosition) {
                    $constraints[] = $query->contains('positions', $filterItem);
                }
                if ($filterItem instanceof AddressOrganisation) {
                    $constraints[] = $query->contains('organisations', $filterItem);
                }
                if (is_string($filterItem)) {
                    $constraints[] = $query->like('name', '%' . $filterItem . '%');
                }
                if (is_array($filterItem)) {
                    // Search for a custom string
                    if (isset($filterItem['customString'])) {
                        if (!empty($filterItem['customString']['value'])) {
                            $searchFields = GeneralUtility::trimExplode(',', $filterItem['customString']['field'] ?? '');
                            $subConstraints = [];
                            foreach ($searchFields as $searchField) {
                                $subConstraints[] = $query->like($searchField, '%' . $filterItem['customString']['value'] . '%');
                            }
                            $constraints[] = $query->logicalOr(...$subConstraints);
                            unset($subConstraints);
                        }
                    } else {
                        // Items found
                        if (count($filterItem) > 0) {
                            $constraints[] = $query->in('uid', $filterItem);
                        } else {
                            // Otherwise there can't be a match!
                            $constraints[] = $query->equals('uid', -99);
                        }
                    }
                }
            }
        }
        return $constraints;
    }

    /**
     * Finds all address uids, which are in distance to the giving geo coordinates
     *
     * @param array<string, mixed> $arguments
     * @return array<mixed>|QueryResultInterface
     */
    public function findByLocation(array $arguments)
    {
        $languageAspect = GeneralUtility::makeInstance(Context::class)->getAspect('language');
        //
        // Validate geo coordinates
        $latitude = 0.0;
        if (isset($arguments['latitude'])) {
            $latitude = (float)$arguments['latitude'];
        }
        $longitude = 0.0;
        if (isset($arguments['longitude'])) {
            $longitude = (float)$arguments['longitude'];
        }
        //
        // Search distance
        $distance = isset($arguments['distance']) ? (int)$arguments['distance'] : 50;
        //
        // To search by kilometers instead of miles, replace 3959 with 6371.
        $distanceUnit = 6371;
        if (!empty($arguments['inMiles'])) {
            $distanceUnit = 3959;
        }
        //
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_addressmanager_domain_model_address');
        $queryBuilder->selectLiteral(
            '*',
            '(' . $distanceUnit . ' * acos
                    (
                        cos(
                            radians(' . $latitude . ')
                        )
                        * cos(
                            radians(map_latitude)
                        ) * cos(
                            radians(map_longitude) - radians(' . $longitude . ')
                        ) + sin(
                            radians(' . $latitude . ')
                        ) * sin(
                            radians(map_latitude)
                        )
                    )
                ) AS distance'
        )
            ->from('tx_addressmanager_domain_model_address')
            ->orWhere(
                $queryBuilder->expr()->eq(
                    'sys_language_uid',
                    $queryBuilder->createNamedParameter($languageAspect->getId(), PDO::PARAM_INT)
                ),
                $queryBuilder->expr()->eq(
                    'sys_language_uid',
                    $queryBuilder->createNamedParameter(-1, PDO::PARAM_INT)
                )
            )
            ->having(
                $queryBuilder->expr()->lte(
                    'distance',
                    $queryBuilder->createNamedParameter($distance, PDO::PARAM_INT)
                )
            )
            ->orderBy('distance', 'ASC');
        /** @var Result $result */
        $result =$queryBuilder->execute();
        return $this->getDataMapper()->map(Address::class, $result->fetchAll());
    }

    /**
     * @param QueryResult $groups
     * @param QueryResult $positions
     * @param QueryResult $organisations
     * @param string $sortBy
     * @param string $order
     * @param int $offset
     * @param int $limit
     * @return array<mixed>|QueryResultInterface
     * @throws InvalidQueryException
     */
    public function findAllForInit(
        QueryResult $groups,
        QueryResult $positions,
        QueryResult $organisations,
        $sortBy = 'name',
        $order = 'ASC',
        $offset = 0,
        $limit = 0
    ) {
        $query = $this->createQuery();
        $constraints = [];
        if ($groups->count() > 0) {
            $constraintsGroups = [];
            foreach ($groups as $group) {
                $constraintsGroups[] = $query->contains('groups', $group);
            }
            $constraints[] = $query->logicalOr(...$constraintsGroups);
        }
        if ($positions->count() > 0) {
            $constraintsPositions = [];
            foreach ($positions as $position) {
                $constraintsPositions[] = $query->contains('positions', $position);
            }
            $constraints[] = $query->logicalOr(...$constraintsPositions);
        }
        if ($organisations->count() > 0) {
            $constraintsOrganisations = [];
            foreach ($organisations as $organisation) {
                $constraintsOrganisations[] = $query->contains('organisations', $organisation);
            }
            $constraints[] = $query->logicalOr(...$constraintsOrganisations);
        }
        if (!empty($constraints)) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        }
        // Sort by start date
        if ($order == 'ASC') {
            $orderings = [$sortBy => QueryInterface::ORDER_ASCENDING];
            if ($sortBy === 'genderThenName') {
                $orderings = ['gender' => QueryInterface::ORDER_ASCENDING, 'name' => QueryInterface::ORDER_ASCENDING];
            }
            if ($sortBy === 'cityThenName') {
                $orderings = ['city' => QueryInterface::ORDER_ASCENDING, 'name' => QueryInterface::ORDER_ASCENDING];
            }
            if ($sortBy === 'lastnameThenFirstname') {
                $orderings = ['lastName' => QueryInterface::ORDER_ASCENDING, 'firstName' => QueryInterface::ORDER_ASCENDING];
            }
            $query->setOrderings($orderings);
        } else {
            $orderings = [$sortBy => QueryInterface::ORDER_DESCENDING];
            if ($sortBy === 'genderThenName') {
                $orderings = ['gender' => QueryInterface::ORDER_DESCENDING, 'name' => QueryInterface::ORDER_DESCENDING];
            }
            if ($sortBy === 'cityThenName') {
                $orderings = ['city' => QueryInterface::ORDER_DESCENDING, 'name' => QueryInterface::ORDER_DESCENDING];
            }
            if ($sortBy === 'lastnameThenFirstname') {
                $orderings = ['lastName' => QueryInterface::ORDER_DESCENDING, 'firstName' => QueryInterface::ORDER_DESCENDING];
            }
            $query->setOrderings($orderings);
        }
        // Use the offset, if available
        if ($offset > 0) {
            $query->setOffset($offset);
        }
        // Use the limit, if available
        if ($limit > 0) {
            $query->setLimit($limit);
        }
        return $query->execute();
    }

    /**
     * @param QueryResult $groups
     * @param QueryResult $positions
     * @param QueryResult $organisations
     * @return int
     * @throws InvalidQueryException
     */
    public function countAllForInit(QueryResult $groups, QueryResult $positions, QueryResult $organisations)
    {
        $query = $this->createQuery();
        $constraints = [];
        if ($groups->count() > 0) {
            $constraintsGroups = [];
            foreach ($groups as $group) {
                $constraintsGroups[] = $query->contains('groups', $group);
            }
            $constraints[] = $query->logicalOr(...$constraintsGroups);
        }
        if ($positions->count() > 0) {
            $constraintsPositions = [];
            foreach ($positions as $position) {
                $constraintsPositions[] = $query->contains('positions', $position);
            }
            $constraints[] = $query->logicalOr(...$constraintsPositions);
        }
        if ($organisations->count() > 0) {
            $constraintsOrganisations = [];
            foreach ($organisations as $organisation) {
                $constraintsOrganisations[] = $query->contains('organisations', $organisation);
            }
            $constraints[] = $query->logicalOr(...$constraintsOrganisations);
        }
        if (!empty($constraints)) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        }
        return $query->execute()->count();
    }

    /**
     * @param string $name
     * @param bool $respectStoragePage
     * @return object|null
     */
    public function findOneByName(string $name, bool $respectStoragePage = true)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage($respectStoragePage);
        $query->matching($query->equals('name', $name));
        return $query->execute()->getFirst();
    }

    /**
     * @param int $uid
     * @param bool $respectStoragePage
     * @return object|null
     */
    public function findOneByUid(int $uid, bool $respectStoragePage = true)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage($respectStoragePage);
        $query->matching($query->equals('uid', $uid));
        return $query->execute()->getFirst();
    }

    /**
     * @param array<mixed> $filter
     * @param bool $count
     * @return array<mixed>|QueryResultInterface|int
     */
    public function findAllForBackendList(array $filter = [], bool $count = false)
    {
        $query = $this->createQuery();
        $constraints = [];
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        if (!empty($filter['searchWord'])) {
            $constraintsSearchWord = [];
            $constraintsSearchWord[] = $query->like('name', '%' . $filter['searchWord'] . '%');
            $constraintsSearchWord[] = $query->like('email', '%' . $filter['searchWord'] . '%');
            $constraints[] = $query->logicalOr(...$constraintsSearchWord);
        }
        if (!empty($filter['group']['selected']) && $filter['group']['selected'] > 0) {
            $constraints[] = $query->contains('groups', $filter['group']['selected']);
        }
        if (!empty($filter['organisation']['selected']) && $filter['organisation']['selected'] > 0) {
            $constraints[] = $query->contains('organisations', $filter['organisation']['selected']);
        }
        if (!empty($filter['position']['selected']) && $filter['position']['selected'] > 0) {
            $constraints[] = $query->contains('positions', $filter['position']['selected']);
        }
        if (count($constraints) > 0) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        }
        if (!$count) {
            if (!empty($filter['sortingField'])) {
                if ($filter['sortingOrder'] == 'asc') {
                    $query->setOrderings([(string)$filter['sortingField'] => QueryInterface::ORDER_ASCENDING]);
                } else {
                    if ($filter['sortingOrder'] == 'desc') {
                        $query->setOrderings([(string)$filter['sortingField'] => QueryInterface::ORDER_DESCENDING]);
                    }
                }
            }
            if (!empty($filter['limit'])) {
                $query->setOffset((int)($filter['offset'] ?? 0));
                $query->setLimit((int)$filter['limit']);
            }
            return $query->execute();
        }
        return $query->execute()->count();
    }

    /**
     * @param array<mixed> $filter
     * @param bool $count
     * @return array<mixed>|QueryResultInterface|int
     */
    public function findAllForFrontendList(array $filter = [], $count = false)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        $constraints = [];
        if (!empty($filter['frontendUser'])) {
            $constraints[] = $query->equals('frontendUser', $filter['frontendUser']->getUid());
            $query->matching($constraints[0]);
        }
        if (!$count) {
            if (!empty($filter['sortingField'])) {
                if ($filter['sortingOrder'] === 'asc') {
                    $query->setOrderings([(string)$filter['sortingField'] => QueryInterface::ORDER_ASCENDING]);
                } else {
                    if ($filter['sortingOrder'] === 'desc') {
                        $query->setOrderings([(string)$filter['sortingField'] => QueryInterface::ORDER_DESCENDING]);
                    }
                }
            }
            if (!empty($filter['limit'])) {
                $query->setOffset((int)($filter['offset'] ?? 0));
                $query->setLimit((int)$filter['limit']);
            }
            return $query->execute();
        }
        return $query->execute()->count();
    }

    /**
     * @param int $uid
     * @param int $frontendUser
     * @return object|null
     */
    public function findByIdentifierFrontend(int $uid, int $frontendUser)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        $constraints = [];
        $constraints[] = $query->equals('uid', $uid);
        $constraints[] = $query->equals('frontendUser', $frontendUser);
        $query->matching($query->logicalAnd(...$constraints));
        return $query->execute()->getFirst();
    }

    /**
     * Returns all matching records for the given list of uids and applies the uidList sorting for the result
     *
     * @param array<int, int> $uids
     * @return array<mixed>
     */
    public function findByUids(array $uids): array
    {
        $addresses = [];
        if (count($uids) > 0) {
            //
            // We need to use the QueryBuilder, because otherwise we can't use such orderBy
            /** @var ConnectionPool $connectionPool */
            $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
            $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_addressmanager_domain_model_address');
            $queryBuilder->select('*')->from('tx_addressmanager_domain_model_address');
            //
            // Respect current language
            /** @var Context $context */
            $context = GeneralUtility::makeInstance(Context::class);
            /** @var LanguageAspect $languageAspect */
            $languageAspect = $context->getAspect('language');
            $languageUid = $languageAspect->getId();
            if ($languageUid !== 0) {
                $queryBuilder->where($queryBuilder->expr()->in('l10n_parent', $uids));
                $queryBuilder->andWhere('(sys_language_uid = ' . $languageUid . ' OR sys_language_uid = -1)');
                $queryBuilder->add('orderBy', 'FIELD(l10n_parent,' . implode(',', $uids) . ')');
            } else {
                $queryBuilder->where($queryBuilder->expr()->in('uid', $uids));
                $queryBuilder->add('orderBy', 'FIELD(uid,' . implode(',', $uids) . ')');
            }
            //
            /** @var Result $result */
            $result = $queryBuilder->execute();
            $addresses = $this->getDataMapper()->map(Address::class, $result->fetchAll());
        }
        return $addresses;
    }

    /**
     * @param int $uid
     * @param string $latitude
     * @param string $longitude
     * @return void
     */
    public function updateAddressLatitudeLongitude(int $uid, string $latitude, string $longitude)
    {
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_addressmanager_domain_model_address');
        $queryBuilder->update('tx_addressmanager_domain_model_address')
            ->where(
                $queryBuilder->expr()->eq(
                    'uid',
                    $queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT)
                )
            )
            ->set('map_latitude', $latitude)
            ->set('map_longitude', $longitude)
            ->executeStatement();
    }
}
