<?php

namespace CodingMs\AddressManager\Domain\Repository;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class AddressPositionRepository extends Repository
{
    /**
     * Find all Address-Positions by allowed positions array
     *
     * @param array<int, int|string> $allowedPositions Array with allowed position uids
     * @param string $sortBy
     * @param string $sortOrder
     * @return array<mixed>|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllByAllowedPositions($allowedPositions = [], $sortBy='sorting', $sortOrder='asc')
    {
        $query = $this->createQuery();
        if (!empty($allowedPositions)) {
            $positionConstraints = [];
            foreach ($allowedPositions as $allowedPositionUid) {
                $allowedPositionUid = (int)$allowedPositionUid;
                if ($allowedPositionUid > 0) {
                    $positionConstraints[] = $query->equals('uid', $allowedPositionUid);
                }
            }
            if (!empty($positionConstraints)) {
                $query->matching($query->logicalOr(...$positionConstraints));
            }
        }
        if ($sortOrder === 'desc') {
            $query->setOrderings([$sortBy => QueryInterface::ORDER_DESCENDING]);
        } else {
            $query->setOrderings([$sortBy => QueryInterface::ORDER_ASCENDING]);
        }
        return $query->execute();
    }

    /**
     * @param array<string, mixed> $filter
     * @param bool $count
     * @return array<mixed>|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface|int
     */
    public function findAllForBackendList(array $filter = [], bool $count = false)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        if (!empty($filter['searchWord'])) {
            $constraints = [];
            $constraints[] = $query->like('title', '%' . $filter['searchWord'] . '%');
            $constraints[] = $query->like('description', '%' . $filter['searchWord'] . '%');
            $query->matching($query->logicalOr(...$constraints));
        }
        if (!$count) {
            if (!empty($filter['sortingField'])) {
                if ($filter['sortingOrder'] == 'asc') {
                    $query->setOrderings([(string)$filter['sortingField'] => QueryInterface::ORDER_ASCENDING]);
                } else {
                    if ($filter['sortingOrder'] == 'desc') {
                        $query->setOrderings([(string)$filter['sortingField'] => QueryInterface::ORDER_DESCENDING]);
                    }
                }
            }
            if (!empty($filter['limit'])) {
                $query->setOffset((int)($filter['offset'] ?? 0));
                $query->setLimit((int)$filter['limit']);
            }
            return $query->execute();
        }
        return $query->execute()->count();
    }
}
